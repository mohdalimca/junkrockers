import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';

callUnfocus({@required BuildContext context}) =>
    FocusScope.of(context).requestFocus(new FocusNode());

Widget textHelper(
        {String title,
        Color textColor,
        double fontSize,
        bool isBold = false,
        bool isItalic = false}) =>
    Text(
      title,
      style: TextStyle(
          fontSize: fontSize,
          color: textColor,
          fontWeight: isBold ? FontWeight.w600 : FontWeight.normal,
          fontStyle: isItalic ? FontStyle.italic : FontStyle.normal),
    );

Widget assetImageHelper({String image, double height, double width}) =>
    Container(
      height: height,
      width: width,
      child: Image.asset(
        image,
        fit: BoxFit.fill,
      ),
    );

Widget appBarBody({
  String title,
  Color textColor,
  String leadingIcon,
  Function onTapLeading,
  String action1Icon,
  Function onTapaction1,
  String action2Icon,
  Function onTapaction2,
  Color bgColor = Colors.transparent,
}) =>
    AppBar(
      backgroundColor: bgColor,
      elevation: 0,
      centerTitle: true,
      //title
      title: Text(
        title,
        style: TextStyle(
            fontSize: 17,
            color: textColor ?? Colors.black,
            fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
      //leading Icon
      leading: InkWell(
        onTap: onTapLeading,
        child: leadingIcon != null
            ? Container(
                margin: EdgeInsets.all(10),
                child: Image.asset(
                  leadingIcon,
                ))
            : Container(
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                margin: EdgeInsets.all(10),
                child: Image.asset(
                  ImageAssets.ic_back,
                  fit: BoxFit.fill,
                )),
      ),
      //Action icon
      actions: <Widget>[
        action1Icon != null
            ? InkWell(
                borderRadius: BorderRadius.circular(30),
                onTap: onTapaction1,
                child: Container(
                    height: 30,
                    width: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      action1Icon,
                    )),
              )
            : Container(
                height: 0,
                width: 0,
              ),
        SizedBox(
          width: 5,
        ),
        action2Icon != null
            ? InkWell(
                borderRadius: BorderRadius.circular(50),
                onTap: onTapaction2,
                child: Container(
                    height: 30,
                    width: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      action2Icon,
                    )),
              )
            : Container(
                height: 0,
                width: 0,
              ),
        SizedBox(
          width: 15,
        )
      ],
    );

Widget signUpformFeild(
        {String tilte,
        String validation(String val),
        String save(String val),
        TextEditingController controller,
        textInputType = TextInputType.text,
        int maxLine = 1,
        FocusNode focusNode,
        FocusNode nextFocusNode,
        int maxLength = 100,
        obscureText = false,
        textCapitalization = TextCapitalization.sentences,
        TextInputAction textInputAction,
        showEye = false,
        Function showEyeFunction,
        BuildContext context}) =>
    Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
              child: TextFormField(
                textAlign: TextAlign.start,
                maxLines: maxLine,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(maxLength),
                ],
                controller: controller,
                keyboardType: textInputType,
                validator: validation,
                onSaved: save,
                obscureText: obscureText,
                focusNode: focusNode,
                textInputAction: textInputAction,
                onFieldSubmitted: (str) => nextFocusNode != null
                    ? FocusScope.of(context).requestFocus(nextFocusNode)
                    : FocusScope.of(context).requestFocus(new FocusNode()),
                textCapitalization: textCapitalization,
                decoration: InputDecoration(
                  labelText: tilte,
                  labelStyle: TextStyle(color: ColorsHelper.greyTextBoxColor()),
                  enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: ColorsHelper.greyTextBoxColor()),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: ColorsHelper.blueButtonColor()),
                  ),
                ),
              ),
            ),
            showEye
                ? InkWell(
                    onTap: showEyeFunction,
                    child: Container(
                        padding: EdgeInsets.all(5),
                        child: FaIcon(
                          obscureText
                              ? FontAwesomeIcons.eye
                              : FontAwesomeIcons.eyeSlash,
                          color: ColorsHelper.blueButtonColor(),
                        )))
                : Container()
          ],
        )
      ],
    );

Widget submitButton({String title, Function onPressed}) => ButtonColor(
    onPressed: onPressed,
    child: Text(
      title,
      style: TextStyle(color: ColorsHelper.whiteColor(), fontSize: 15),
    ));

Widget rowButtonHelper(
        {String back = StringHelper.back,
        String next = StringHelper.next,
        Function onBack,
        Function onNext}) =>
    Row(
      children: <Widget>[
        //back
        Expanded(
            child: submitButton(
          title: back.toUpperCase(),
          onPressed: onBack,
        )),
        SizedBox(
          width: 20,
        ),
        //next
        Expanded(
          child: submitButton(
            title: next.toUpperCase(),
            onPressed: onNext,
          ),
        ),
      ],
    );
