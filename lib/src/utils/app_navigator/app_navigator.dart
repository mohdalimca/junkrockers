import 'package:flutter/material.dart';
import 'package:junkrocker/src/model/BookingDetailModel.dart';
import 'package:junkrocker/src/model/BookingHistoryModel.dart';
import 'package:junkrocker/src/model/PagesModel.dart';
import 'package:junkrocker/src/ui_screens/booking_steps_module/step_2.dart';
import 'package:junkrocker/src/ui_screens/booking_steps_module/step_3.dart';
import 'package:junkrocker/src/ui_screens/booking_steps_module/step_4.dart';
import 'package:junkrocker/src/ui_screens/booking_steps_module/step_5.dart';
import 'package:junkrocker/src/ui_screens/booking_steps_module/step_6.dart';
import 'package:junkrocker/src/ui_screens/home_module/notifications.dart';
import 'package:junkrocker/src/ui_screens/on_borading_module/welcome.dart';
import 'package:junkrocker/src/ui_screens/profile_module/booking_history.dart';
import 'package:junkrocker/src/ui_screens/profile_module/change_password.dart';
import 'package:junkrocker/src/ui_screens/profile_module/edit_profile.dart';
import 'package:junkrocker/src/ui_screens/profile_module/profile.dart';
import 'package:junkrocker/src/ui_screens/profile_module/slug_pages.dart';
import 'package:junkrocker/src/ui_screens/splash/Splash.dart';
import 'package:junkrocker/src/ui_screens/on_borading_module/ForgotPassword.dart';
import 'package:junkrocker/src/ui_screens/on_borading_module/Login.dart';
import 'package:junkrocker/src/ui_screens/on_borading_module/register.dart';

import 'package:junkrocker/src/ui_screens/home_module/Home.dart';
import 'package:junkrocker/src/ui_screens/upcoming_bookings/upcoming_bookings.dart';

class AppNavigator {
  //WelcomeScreen
  static void launchWelcomeScreen(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Welcome.routeName, (Route<dynamic> route) => false);
  }

  //LoginScreen
  static void launchLoginScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Login.routeName,
    );
  }

  //ForgotPassword
  static void launchForgotPasswordScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      ForgotPassword.routeName,
    );
  }

  //Register
  static void launchRegisterScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Register.routeName,
    );
  }

  static void launchSplashPage(BuildContext context) {
    Navigator.pushReplacementNamed(
      context,
      Splash.routeName,
    );
  }

  //HomeScreen
  static void launchHomeScreen(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Home.routeName, (Route<dynamic> route) => false);
  }

  //Profile
  static void launchProfileScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Profile.routeName,
    );
  }

  //EditProfile
  static void launchEditProfileScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      EditProfile.routeName,
    );
  }

  //BookingHistory
  static void launchBookingHistoryScreen(
      BuildContext context, List<BookingHistoryData> bookingHistoryList) {
    Navigator.pushNamed(context, BookingHistory.routeName,
        arguments: bookingHistoryList);
  }

  //Step2Booking
  static void launchStep2BookingScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Step2Booking.routeName,
    );
  }

  //Step3Booking
  static void launchStep3BookingScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Step3Booking.routeName,
    );
  }

  //Step4Booking
  static void launchStep4BookingScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Step4Booking.routeName,
    );
  }

  //Step5Booking
  static void launchStep5BookingScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Step5Booking.routeName,
    );
  }

  //Step6Booking
  static void launchStep6BookingScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Step6Booking.routeName,
    );
  }

  //UpcomingBookings
  static void launchUpcomingBookingsScreen(
      BuildContext context, BookingDetailData bookingDetailData) {
    Navigator.pushNamed(context, UpcomingBookings.routeName,
        arguments: bookingDetailData);
  }

  //SlugPage
  static void launchSlugPageScreen(BuildContext context, PagesData pageData) {
    Navigator.pushNamed(context, SlugPage.routeName, arguments: pageData);
  }

  //Notifications
  static void launchNotificationsScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Notifications.routeName,
    );
  }

  //ChangePassword
  static void launchChangePasswordScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      ChangePassword.routeName,
    );
  }

  static void popBackStack(BuildContext context) {
    Navigator.pop(context);
  }
}
