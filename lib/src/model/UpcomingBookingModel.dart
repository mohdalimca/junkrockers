class UpcomingBookingModel {
  bool status;
  String message;
  List<UpcomingBookingData> data;

  UpcomingBookingModel({this.status, this.message, this.data});

  UpcomingBookingModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<UpcomingBookingData>();
      json['data'].forEach((v) {
        data.add(new UpcomingBookingData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UpcomingBookingData {
  String bookingId;
  String bookingType;
  String price;
  String itemTypeProduct;
  String itemType;
  String bookingDate;

  UpcomingBookingData(
      {this.bookingId,
      this.bookingType,
      this.price,
      this.itemTypeProduct,
      this.itemType,
      this.bookingDate});

  UpcomingBookingData.fromJson(Map<String, dynamic> json) {
    bookingId = json['bookingId'];
    bookingType = json['booking_type'];
    price = json['price'];
    itemTypeProduct = json['item_type_product'];
    itemType = json['item_type'];
    bookingDate = json['booking_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bookingId'] = this.bookingId;
    data['booking_type'] = this.bookingType;
    data['price'] = this.price;
    data['item_type_product'] = this.itemTypeProduct;
    data['item_type'] = this.itemType;
    data['booking_date'] = this.bookingDate;
    return data;
  }
}
