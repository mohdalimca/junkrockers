class LoginModel {
  bool status;
  String message;
  String token;
  LoginData data;

  LoginModel({this.status, this.message, this.token, this.data});

  LoginModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    token = json['token'];
    data = json['data'] != null ? new LoginData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['token'] = this.token;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class LoginData {
  String id;
  String roleId;
  String fullName;
  String email;
  String profileImage;
  String password;
  String phone;
  String created;
  String modified;
  String updatedBy;
  String status;
  String lastLogin;

  LoginData(
      {this.id,
      this.roleId,
      this.fullName,
      this.email,
      this.profileImage,
      this.password,
      this.phone,
      this.created,
      this.modified,
      this.updatedBy,
      this.status,
      this.lastLogin});

  LoginData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roleId = json['role_id'];
    fullName = json['full_name'];
    email = json['email'];
    profileImage = json['profile_image'];
    password = json['password'];
    phone = json['phone'];
    created = json['created'];
    modified = json['modified'];
    updatedBy = json['updatedBy'];
    status = json['status'];
    lastLogin = json['lastLogin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['role_id'] = this.roleId;
    data['full_name'] = this.fullName;
    data['email'] = this.email;
    data['profile_image'] = this.profileImage;
    data['password'] = this.password;
    data['phone'] = this.phone;
    data['created'] = this.created;
    data['modified'] = this.modified;
    data['updatedBy'] = this.updatedBy;
    data['status'] = this.status;
    data['lastLogin'] = this.lastLogin;
    return data;
    
  }
}



