class PagesModel {
  bool status;
  String message;
  List<PagesData> data;

  PagesModel({this.status, this.message, this.data});

  PagesModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<PagesData>();
      json['data'].forEach((v) {
        data.add(new PagesData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PagesData {
  String id;
  String slug;
  String pageTitle;
  String pageContent;

  PagesData({this.id, this.slug, this.pageTitle, this.pageContent});

  PagesData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    slug = json['slug'];
    pageTitle = json['page_title'];
    pageContent = json['page_content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['slug'] = this.slug;
    data['page_title'] = this.pageTitle;
    data['page_content'] = this.pageContent;
    return data;
  }
}