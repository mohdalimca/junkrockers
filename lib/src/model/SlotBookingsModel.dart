class SlotBookingsModel {
  bool status;
  String message;
  List<SlotBookingsData> data;

  SlotBookingsModel({this.status, this.message, this.data});

  SlotBookingsModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<SlotBookingsData>();
      json['data'].forEach((v) {
        data.add(new SlotBookingsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SlotBookingsData {
  String slotId;
  String slotDate;
  String startTime;
  String endTime;
  String slotTitle;
  String status;
  String isBooked;
  String addeddate;
  String created;

  SlotBookingsData(
      {this.slotId,
      this.slotDate,
      this.startTime,
      this.endTime,
      this.slotTitle,
      this.status,
      this.isBooked,
      this.addeddate,
      this.created});

  SlotBookingsData.fromJson(Map<String, dynamic> json) {
    slotId = json['slot_id'];
    slotDate = json['slot_date'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    slotTitle = json['slot_title'];
    status = json['status'];
    isBooked = json['isBooked'];
    addeddate = json['addeddate'];
    created = json['created'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['slot_id'] = this.slotId;
    data['slot_date'] = this.slotDate;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['slot_title'] = this.slotTitle;
    data['status'] = this.status;
    data['isBooked'] = this.isBooked;
    data['addeddate'] = this.addeddate;
    data['created'] = this.created;
    return data;
  }
}