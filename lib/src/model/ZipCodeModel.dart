class ZipCodeModel {
  bool status;
  String message;
  List<ZipCodeData> data;

  ZipCodeModel({this.status, this.message, this.data});

  ZipCodeModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<ZipCodeData>();
      json['data'].forEach((v) {
        data.add(new ZipCodeData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ZipCodeData {
  String pid;
  String zipcode;

  ZipCodeData({this.pid, this.zipcode});

  ZipCodeData.fromJson(Map<String, dynamic> json) {
    pid = json['pid'];
    zipcode = json['zipcode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pid'] = this.pid;
    data['zipcode'] = this.zipcode;
    return data;
  }
}