class BookingHistoryModel {
  bool status;
  String message;
  List<BookingHistoryData> data;

  BookingHistoryModel({this.status, this.message, this.data});

  BookingHistoryModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<BookingHistoryData>();
      json['data'].forEach((v) {
        data.add(new BookingHistoryData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BookingHistoryData {
  String bookingId;
  String bookingType;
  String price;
  String itemTypeProduct;
  String itemType;
  String transactionNumber;
  String bookingDate;

  BookingHistoryData(
      {this.bookingId,
      this.bookingType,
      this.price,
      this.itemTypeProduct,
      this.itemType,
      this.transactionNumber,
      this.bookingDate});

  BookingHistoryData.fromJson(Map<String, dynamic> json) {
    bookingId = json['bookingId'];
    bookingType = json['booking_type'];
    price = json['price'];
    itemTypeProduct = json['item_type_product'];
    itemType = json['item_type'];
    transactionNumber = json['transaction_number'];
    bookingDate = json['booking_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bookingId'] = this.bookingId;
    data['booking_type'] = this.bookingType;
    data['price'] = this.price;
    data['item_type_product'] = this.itemTypeProduct;
    data['item_type'] = this.itemType;
    data['transaction_number'] = this.transactionNumber;
    data['booking_date'] = this.bookingDate;
    return data;
  }
}
