class BookingDetailModel {
  bool status;
  String message;
  List<BookingDetailData> data;

  BookingDetailModel({this.status, this.message, this.data});

  BookingDetailModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<BookingDetailData>();
      json['data'].forEach((v) {
        data.add(new BookingDetailData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BookingDetailData {
  String bookingId;
  String trackingId;
  String bookingType;
  String itemType;
  String itemTypeProduct;
  String bookingDate;
  String price;
  String arrivalDatetime;
  String arrivalDate;
  String arrivalTime;
  String trackStatus;
  String junkerFullname;
  String junkeremail;
  String junkerprofile;

  BookingDetailData(
      {this.bookingId,
      this.trackingId,
      this.bookingType,
      this.itemType,
      this.itemTypeProduct,
      this.bookingDate,
      this.price,
      this.arrivalDatetime,
      this.arrivalDate,
      this.arrivalTime,
      this.trackStatus,
      this.junkerFullname,
      this.junkeremail,
      this.junkerprofile});

  BookingDetailData.fromJson(Map<String, dynamic> json) {
    bookingId = json['bookingId'];
    trackingId = json['trackingId'];
    bookingType = json['booking_type'];
    itemType = json['item_type'];
    itemTypeProduct = json['item_type_product'];
    bookingDate = json['booking_date'];
    price = json['price'];
    arrivalDatetime = json['arrival_datetime'];
    arrivalDate = json['arrival_date'];
    arrivalTime = json['arrival_time'];
    trackStatus = json['track_status'];
    junkerFullname = json['junker_fullname'];
    junkeremail = json['junkeremail'];
    junkerprofile = json['junkerprofile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bookingId'] = this.bookingId;
    data['trackingId'] = this.trackingId;
    data['booking_type'] = this.bookingType;
    data['item_type'] = this.itemType;
    data['item_type_product'] = this.itemTypeProduct;
    data['booking_date'] = this.bookingDate;
    data['price'] = this.price;
    data['arrival_datetime'] = this.arrivalDatetime;
    data['arrival_date'] = this.arrivalDate;
    data['arrival_time'] = this.arrivalTime;
    data['track_status'] = this.trackStatus;
    data['junker_fullname'] = this.junkerFullname;
    data['junkeremail'] = this.junkeremail;
    data['junkerprofile'] = this.junkerprofile;
    return data;
  }
}