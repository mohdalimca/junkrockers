class BookingQueModel {
  bool status;
  String message;
  List<BookingQueData> data;

  BookingQueModel({this.status, this.message, this.data});

  BookingQueModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<BookingQueData>();
      json['data'].forEach((v) {
        data.add(new BookingQueData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BookingQueData {
  int step;
  String question;
  List<Answer> answer;

  BookingQueData({this.step, this.question, this.answer});

  BookingQueData.fromJson(Map<String, dynamic> json) {
    step = json['step'];
    question = json['question'];
    if (json['answer'] != null) {
      answer = new List<Answer>();
      json['answer'].forEach((v) {
        answer.add(new Answer.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['step'] = this.step;
    data['question'] = this.question;
    if (this.answer != null) {
      data['answer'] = this.answer.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Answer {
  String id;
  String quesId;
  String optionName;

  Answer({this.id, this.quesId, this.optionName});

  Answer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    quesId = json['ques_id'];
    optionName = json['option_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ques_id'] = this.quesId;
    data['option_name'] = this.optionName;
    return data;
  }
}
