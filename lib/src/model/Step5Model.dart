class Step5Model {
  bool status;
  String message;
  List<Step5Data> data;

  Step5Model({this.status, this.message, this.data});

  Step5Model.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Step5Data>();
      json['data'].forEach((v) {
        data.add(new Step5Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Step5Data {
  String itemType;
  String totalItems;
  String estimatorTitle;
  List<Images> images;

  Step5Data({this.itemType, this.totalItems, this.estimatorTitle, this.images});

  Step5Data.fromJson(Map<String, dynamic> json) {
    itemType = json['item_type'];
    totalItems = json['total_items'];
    estimatorTitle = json['estimator_title'];
    if (json['images'] != null) {
      images = new List<Images>();
      json['images'].forEach((v) {
        images.add(new Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['item_type'] = this.itemType;
    data['total_items'] = this.totalItems;
    data['estimator_title'] = this.estimatorTitle;
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Images {
  String id;
  String estimatorId;
  String title;
  String image;
  String price;

  Images({this.id, this.estimatorId, this.title, this.image, this.price});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estimatorId = json['estimator_id'];
    title = json['title'];
    image = json['image'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estimator_id'] = this.estimatorId;
    data['title'] = this.title;
    data['image'] = this.image;
    data['price'] = this.price;
    return data;
  }
}
