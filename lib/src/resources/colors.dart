import 'dart:ui';

import 'package:flutter/material.dart';

class ColorsHelper {
  static Color whiteColor() => Color(0xffFFFFFF);

  static Color backgroundColor() => Color(0xffF7F7F7);

  static Color blueButtonColor() => Color(0xff1B2C55);

  static Color greyTextBoxColor() => Color(0xffC4CDD9);

  static Color greySubHeadColor() => Color(0xff9B9B9B);

  static Color redishTextColor() => Color(0xffDE384F);

  static Color greenDollerColor() => Color(0xff349434);

  static Color darkBlueColor() => Color(0xff303030);

  static LinearGradient linerGradient() => LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [darkBlueColor(), whiteColor()]);
}
