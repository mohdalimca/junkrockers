class StringHelper {
  static const app_name = 'Junk Rockers';

  static const playStoreUrl = "https://play.google.com/store/apps/";
  static const press_back_again_to_exit = "Press back again to exit";
  static const str_camera = "Camera";
  static const str_gallery = "Gallery";
  static const str_choose_from = "Choose image form";

  //Pages Slug
  static const about_us_Slug = 'about_us';
  static const privacy_policy_Slug = 'privacy_policy';
  static const term_and_condition_Slug = 'term_and_condition';

  //Welcome Screen
  static const login = 'Login';
  static const register = 'Register';

  //Login
  static const welcome_back = 'Welcome Back!';
  static const email = 'Email';
  static const password = 'Password';
  static const remember_me = 'Remember Me';
  static const forgot_Password = 'Forgot Password';
  static const login_Successfully = 'Login Successfully';

  //Forgotpassword
  static const new_Password_Will_be_sent =
      'New Password Will be sent to your\nregistered email id';
  static const submit = 'Submit';

  //Register
  static const join_the_Community = 'Join the Community';
  static const name = 'Name';
  static const confirm_Password = 'Confirm Password';
  static const password_and_Confirm_Password_Do_not_match =
      'Password and Confirm Password Do not match';
  static const please_login_using_your_new_credential =
      'Please login using your new credential';

  //Home
  static const no_Hassle_Up_Fornt_Online =
      'No Hassle Up Fornt Online Estimate Don\'t Wait For An On-Site Estimate Book Online Now';
  static const enter_zipCode = 'Enter Zip Code';
  static const check = 'Check';
  static const no_credit_card_required = '*No credit card required';
  static const customer_Services_for_hassle_free_Experience =
      '24X7 Customer Services for hassle free Experience';
  static const book_on_call_now = 'Book on call now';
  static const upcoming_Booking = 'Upcoming Booking';
  static const no_upcoming_Booking = 'No upcoming bookings';
  static const no_Bookings_History = 'No Bookings History';
  static const please_enter_ZipCode = 'Please enter ZipCode';
  static const service_not_availble_at_this_ZipCode =
      'Service not availble at this ZipCode';

  //Profile
  static const profile = 'Profile';
  static const booking_history = 'Booking history';
  static const about_us = 'About us';
  static const privacy_policy = 'Privacy policy';
  static const terms_and_conditions = 'Terms and conditions';
  static const logout = 'Logout';

  //Edit profile
  static const edit_profile = 'Edit profile';
  static const update_Image = 'Update Image';
  static const save = 'Save';

  //Booking History
  static const recent_bookings = 'Recent bookings';
  static const transaction_number = 'Transaction number';

  //Notifications
  static const notifications = 'Notifications';

  //Booking Steps module
  static const step2 = 'Step 2 of 6';
  static const step3 = 'Step 3 of 6';
  static const step4 = 'Step 4 of 6';
  static const step5 = 'Step 5 of 6';
  static const step6 = 'Step 6 of 6';
  //step 2
  static const are_There_Any_Large_Items = 'Are There Any Large Items?';
  static const any_Items_of_5_feet_is_considered_large =
      'Any Items of 5 feet is considered large';
  static const select_any_one = 'Select any one';
  static const back = 'Back';
  static const next = 'Next';
  //step 3
  static const what_is_the_primary_type =
      'What is the primary type of junk you need removed?';
  static const furniture_appliances_electronics =
      'Furniture, appliances, electronics';
  static const remodel_demolition_debris = 'Remodel / demolition debris';
  static const miscellaneous_trash_clutter = 'Miscellaneous trash / clutter';
  static const all_of_the_above = 'All of the above';
  //step 4
  static const are_There_Any_Stairs = 'Are There Any Stairs?';
  static const no_Stairs = 'No Stairs';
  static const has_Elevator = 'Has Elevator';
  static const flight_of_stairs = 'flight of stairs';
  //step 5
  static const how_much_junk_do_you_think_needs_to_be_removed =
      'How much junk do you think needs to be removed & hauled away?';
  static const price_estimator = 'Price estimator';
  static const by_list_item = 'By list item';
  static const by_truck_load = 'By truck load';
  static const collapse = 'Collapse';
  static const expand = 'Expand';
  static const estimated_price = 'Estimated price';
  static const minimum = 'Minimum';
  //Step 6
  static const select_slot = 'Select slot';
  static const date_Time = 'Date & Time';
  static const address_details = 'Address details';
  static const home = 'Home';
  static const business = 'Business';
  static const flat_Floor_Appartment = 'Flat / Floor / Appartment';
  static const street = 'Street';
  static const landmark = 'Landmark';
  static const city_State_Pincode = 'City / State / Pincode';
  static const i_will_be_at_the_pick_up_address =
      'I will be at the pick-up address';
  static const please_notify_me_about_promotions =
      'Please notify me about promotions';
  static const pay = 'Pay';
  static const please_Select_a_Time_Slot = 'Please Select a Time Slot';

  //Upcoming Bookings
  static const your_pickup_has_been_accepted_for =
      'Your pickup has been accepted for ';
  static const your_juncker_will_arrive_at =
      'Your juncker will arrive at ';
  static const juncker_in_on_the_way = 'Juncker in on the way';
  static const your_junk_is_gone = 'Your junk is gone!';
  static const completed = 'Completed';  

  //Change Password
  static const change_Password = 'Change Password';
  static const old_password = 'Old Password';

  //No Net Connection
  static const no_Internet_Connection = 'No Internet Connection';
  static const please_try_to_connect_with_internet =
      'Please try to Connect with Internet';

  //Booking Popup
  static const book_Online = 'Book Online';
  static const service = 'service';
  static const available = ' available ';
  static const at = 'at';
  static const edit = 'Edit';
  static const commercial = 'Commercial';
  static const residentail = 'Residentail';

  //PopupDialog
  static const alert = 'Alert';
  static const no = 'No';
  static const yes = 'Yes';
  static const cancel = 'Cancel';
  static const try_again = 'Try Again';
  static const ok = 'Ok';
  static const update = 'Update';
  static const session_expired_please = "Session expired please login again";

  //errorMessages
  static const error_msg_empty_mobile = 'Please enter a mobile number';
  static const error_msg_invalid_mobile_10_digits =
      'Please enter valid 10 digit phone number';
  static const error_msg_empty_pass = 'Please enter a password';
  static const error_msg_min_length =
      'Password should be minimum of 6 characters';
  static const error_msg_empty_otp = 'Please enter OTP';
  static const error_msg_invalid_otp = 'Please enter vaild OTP';
  static const error_msg_empty_email = 'Please enter a email address';
  static const error_msg_invalid_email = 'Please enter a valid email address';
  static const error_empty_value = 'Please fill field';
  static const error_firstname_empty_value = 'Please enter a first name';
  static const error_lastname_empty_value = 'Please enter a last name';
  static const error_msg_empty_confirm_pass = 'Please enter a confirm password';
  static const error_msg_empty_address = 'Please enter a address';
}
