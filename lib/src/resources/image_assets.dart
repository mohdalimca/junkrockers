class ImageAssets {
 
  // Splash
  static const logo_splash = 'images/Logo_splash@3x.png';

  //Welcome
  static const logo_login = 'images/Logo_login@3x.png';

  //Login
  static const ic_back = 'images/ic_back@3x.png';
  static const ic_unselected02 = 'images/ic_unselected02@3x.png';
  static const ic_selected = 'images/ic_selected@3x.png';

  //Home
  static const ic_user = 'images/ic_user@2x.png';
  static const ic_bell = 'images/ic_bell@3x.png';
  static const img_home_bg = 'images/img_home_bg@3x.png';
  static const img_customer_service = 'images/img_customer_service@3x.png';
  static const img_no_booking = 'images/img_no_booking@3x.png';
  static const ic_next = 'images/ic_next@3x.png';

  //profile
  static const ic_profile02 = 'images/ic_profile02@3x.png';
  static const ic_menu_next = 'images/ic_menu_next@3x.png';
  static const logo_aboutus = 'images/Logo_aboutus@3x.png';

  //Booking Steps
  static const ic_info = 'images/ic_info@3x.png';
  static const img_truck = 'images/img_truck@3x.png';
  static const img_junktruck = 'images/img_junktruck@3x.png';

  //Upcomming bookisngs
  static const ic_user_ = 'images/ic_user@3x.png';
  static const ic_call = 'images/ic_call@3x.png';
}
