import 'package:flutter/material.dart';
import 'package:junkrocker/src/bloc/bloc_login/login_bloc.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:junkrocker/src/utils/validator/validator.dart';

class Register extends StatefulWidget {
  static const String routeName = "/Register";
  Register({Key key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool isHideP1 = true, isHideP2 = true;
  final formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPwdController = TextEditingController();

  FocusNode nameNode = FocusNode();
  FocusNode emailNode = FocusNode();
  FocusNode passwordNode = FocusNode();
  FocusNode confirmPwdNode = FocusNode();

  LoginBloc loginBloc = LoginBloc();

  submit() async {
    var result = await loginBloc.callSignUpApi(
        fullName: nameController.text.trim(),
        email: emailController.text.trim(),
        password: passwordController.text.trim(),
        context: context);
    if (result != null) {
      AppNavigator.launchLoginScreen(context);
      PopupDialogs.displayMessageOnly(
          context, StringHelper.please_login_using_your_new_credential);
    }
  }

  @override
  void dispose() {
    loginBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
          title: "",
          leadingIcon: null,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: registerBody(),
        ),
      ),
    );
  }

  Widget registerBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //join_the_Community
              Text(
                StringHelper.join_the_Community,
                style: TextStyle(
                    fontSize: 30, color: ColorsHelper.blueButtonColor()),
              ),
              SizedBox(
                height: 10,
              ),
              //new_Password_Will_be_sent
              Text(
                StringHelper.new_Password_Will_be_sent,
                style: TextStyle(color: ColorsHelper.greySubHeadColor()),
              ),
              SizedBox(
                height: 40,
              ),
              formBody(),
              SizedBox(
                height: 40,
              ),
              //loginButton
              registerButton(),
            ],
          ),
        ),
      );

  Widget formBody() => Form(
      key: formKey,
      autovalidate: loginBloc.autoValidation,
      child: Column(
        children: <Widget>[
          //name
          signUpformFeild(
              tilte: StringHelper.name,
              validation: CommonValidator.emptyValidation,
              controller: nameController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: nameNode,
              nextFocusNode: emailNode,
              textInputAction: TextInputAction.next,
              context: context),
          SizedBox(
            height: 10,
          ),
          //Email
          signUpformFeild(
              tilte: StringHelper.email,
              validation: CommonValidator.emailValidation,
              controller: emailController,
              textInputType: TextInputType.emailAddress,
              maxLine: 1,
              focusNode: emailNode,
              nextFocusNode: passwordNode,
              textInputAction: TextInputAction.next,
              context: context),
          SizedBox(
            height: 10,
          ),
          //Password
          signUpformFeild(
              tilte: StringHelper.password,
              validation: CommonValidator.passValidation,
              controller: passwordController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: passwordNode,
              nextFocusNode: confirmPwdNode,
              textInputAction: TextInputAction.next,
              showEye: true,
              showEyeFunction: () {
                if (mounted) {
                  isHideP1 = !isHideP1;
                  setState(() {});
                }
              },
              obscureText: isHideP1,
              context: context),
          SizedBox(
            height: 10,
          ),
          //confirm_Password
          signUpformFeild(
              tilte: StringHelper.confirm_Password,
              validation: CommonValidator.passValidation,
              controller: confirmPwdController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: confirmPwdNode,
              nextFocusNode: null,
              textInputAction: TextInputAction.done,
              showEye: true,
              showEyeFunction: () {
                if (mounted) {
                  isHideP2 = !isHideP2;
                  setState(() {});
                }
              },
              obscureText: isHideP2,
              context: context)
        ],
      ));

  //register
  Widget registerButton() => ButtonColor(
      onPressed: () {
        callUnfocus(context: context);
        loginBloc.onLoginClick(formKey).then((onValue) {
          if (onValue) {
            if (passwordController.text == confirmPwdController.text) {
              submit();
            } else {
              PopupDialogs.displayMessage(context,
                  StringHelper.password_and_Confirm_Password_Do_not_match);
            }
          } else {
            if (mounted) setState(() {});
          }
        });
      },
      child: Text(
        StringHelper.register.toUpperCase(),
        style: TextStyle(color: ColorsHelper.whiteColor(), fontSize: 15),
      ));
}
