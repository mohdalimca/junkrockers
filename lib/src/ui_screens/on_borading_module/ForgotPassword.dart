import 'package:flutter/material.dart';
import 'package:junkrocker/src/bloc/bloc_login/login_bloc.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:junkrocker/src/utils/validator/validator.dart';

class ForgotPassword extends StatefulWidget {
  static const String routeName = "/ForgotPassword";
  ForgotPassword({Key key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();

  FocusNode emailNode = FocusNode();

  LoginBloc loginBloc = LoginBloc();

  submit() async {
    var result = await loginBloc.callForgotPasswordApi(
        email: emailController.text.trim(), context: context);
    if (result != null) {
      AppNavigator.popBackStack(context);
      PopupDialogs.displayMessageOnly(context, result);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
          title: "",
          leadingIcon: null,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: forgotPasswordBody(),
        ),
      ),
    );
  }

  Widget forgotPasswordBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //forgot_Password
              Text(
                StringHelper.forgot_Password,
                style: TextStyle(
                    fontSize: 30, color: ColorsHelper.blueButtonColor()),
              ),
              SizedBox(
                height: 10,
              ),
              //new_Password_Will_be_sent
              Text(
                StringHelper.new_Password_Will_be_sent,
                style: TextStyle(color: ColorsHelper.greySubHeadColor()),
              ),
              SizedBox(
                height: 40,
              ),
              formBody(),
              SizedBox(
                height: 20,
              ),
              forgotButton()
            ],
          ),
        ),
      );

  Widget formBody() => Form(
      key: formKey,
      autovalidate: loginBloc.autoValidation,
      child: Column(
        children: <Widget>[
          //Email
          signUpformFeild(
              tilte: StringHelper.email,
              validation: CommonValidator.emailValidation,
              controller: emailController,
              textInputType: TextInputType.emailAddress,
              maxLine: 1,
              focusNode: emailNode,
              nextFocusNode: null,
              textInputAction: TextInputAction.done,
              context: context),
          SizedBox(
            height: 10,
          ),
        ],
      ));

  //Login
  Widget forgotButton() => ButtonColor(
      onPressed: () {
        callUnfocus(context: context);
        loginBloc.onLoginClick(formKey).then((onValue) {
          if (onValue) {
            submit();
          } else {
            if (mounted) setState(() {});
          }
        });
      },
      child: Text(
        StringHelper.submit.toUpperCase(),
        style: TextStyle(color: ColorsHelper.whiteColor(), fontSize: 15),
      ));
}
