import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:junkrocker/src/bloc/bloc_login/login_bloc.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:junkrocker/src/utils/validator/validator.dart';

class Login extends StatefulWidget {
  static const String routeName = "/Login";
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _isSavePassword = false, isHide = true;
  final formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  FocusNode emailNode = FocusNode();
  FocusNode passwordNode = FocusNode();

  LoginBloc loginBloc = LoginBloc();

  @override
  void initState() {
    callData();
    super.initState();
  }

  callData() async {
    emailController.text = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Email);
    passwordController.text = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Password);
    _isSavePassword = await SharePreferencesHelper.getInstant()
        .getBool(SharePreferencesHelper.Is_Save_password);
    if (mounted) {
      setState(() {});
    }
  }

  submit() async {
    if (_isSavePassword) {
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Email, emailController.text.trim());
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Password, passwordController.text.trim());
    } else {
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Email, emailController.text.trim());
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Password, '');
    }
    var result = await loginBloc.callLoginApi(
        email: emailController.text.trim(),
        password: passwordController.text.trim(),
        context: context);
    if (result) {
      SharePreferencesHelper.getInstant()
          .setBool(SharePreferencesHelper.Is_Login, true);
      AppNavigator.launchHomeScreen(context);
      Fluttertoast.showToast(
          msg: StringHelper.login_Successfully,
          backgroundColor: ColorsHelper.blueButtonColor(),
          textColor: Colors.white);
    }
  }

  @override
  void dispose() {
    loginBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
          title: "",
          leadingIcon: null,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: loginBody(),
        ),
      ),
    );
  }

  Widget loginBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //welcome_back
              Text(
                StringHelper.welcome_back,
                style: TextStyle(
                    fontSize: 30, color: ColorsHelper.blueButtonColor()),
              ),
              formBody(),
              SizedBox(
                height: 20,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  //remember_password
                  rememberPassword(),
                  //forgot password
                  forgotPassword(),
                ],
              ),

              SizedBox(
                height: 40,
              ),
              //loginButto
              loginButton(),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      );

  Widget formBody() => Form(
      key: formKey,
      autovalidate: loginBloc.autoValidation,
      child: Column(
        children: <Widget>[
          //Email
          signUpformFeild(
              tilte: StringHelper.email,
              validation: CommonValidator.emailValidation,
              controller: emailController,
              textInputType: TextInputType.emailAddress,
              maxLine: 1,
              focusNode: emailNode,
              nextFocusNode: passwordNode,
              textInputAction: TextInputAction.next,
              context: context),
          SizedBox(
            height: 10,
          ),
          //Password
          signUpformFeild(
              tilte: StringHelper.password,
              validation: CommonValidator.passValidation,
              controller: passwordController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: passwordNode,
              nextFocusNode: null,
              textInputAction: TextInputAction.done,
              showEye: true,
              showEyeFunction: () {
                if (mounted) {
                  isHide = !isHide;
                  setState(() {});
                }
              },
              obscureText: isHide,
              context: context)
        ],
      ));

  Widget rememberPassword() => Container(
          child: Row(
        children: <Widget>[
          //CheckBox
          InkWell(
            borderRadius: BorderRadius.circular(10),
            onTap: () {
              callUnfocus(context: context);
              if (mounted)
                setState(() {
                  _isSavePassword = !_isSavePassword;
                  SharePreferencesHelper.getInstant().setBool(
                      SharePreferencesHelper.Is_Save_password, _isSavePassword);
                });
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: _isSavePassword
                    ? Icon(
                        Icons.check_box,
                        color: ColorsHelper.blueButtonColor(),
                        size: 20,
                      )
                    : Icon(
                        Icons.check_box_outline_blank,
                        color: ColorsHelper.blueButtonColor(),
                        size: 20,
                      )),
          ),
          Text(
            StringHelper.remember_me,
            style:
                TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 15),
          ),
        ],
      ));

  Widget forgotPassword() => Container(
      alignment: Alignment.centerRight,
      child: InkWell(
        onTap: () {
          callUnfocus(context: context);
          AppNavigator.launchForgotPasswordScreen(context);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          child: Text(
            StringHelper.forgot_Password + "?",
            style:
                TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 15),
          ),
        ),
      ));

  //Login
  Widget loginButton() => ButtonColor(
      onPressed: () {
        callUnfocus(context: context);
        loginBloc.onLoginClick(formKey).then((onValue) {
          if (onValue) {
            submit();
          } else {
            if (mounted) setState(() {});
          }
        });
      },
      child: Text(
        StringHelper.login.toUpperCase(),
        style: TextStyle(
          color: ColorsHelper.whiteColor(),
          fontSize: 15,
        ),
      ));
}
