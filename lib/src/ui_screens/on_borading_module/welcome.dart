import 'package:flutter/material.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';

class Welcome extends StatefulWidget {
  static const String routeName = "Welcome";
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsHelper.backgroundColor(),
      body: Container(padding: EdgeInsets.all(0), child: welcomeBody()),
    );
  }

  Widget welcomeBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //logo_login
              Container(
                  height: MediaQuery.of(context).size.height * 0.50,
                  alignment: Alignment.bottomCenter,
                  child: Image.asset(
                    ImageAssets.logo_login,
                    fit: BoxFit.fill,
                    height: MediaQuery.of(context).size.width * 0.5,
                    width: MediaQuery.of(context).size.width * 0.5,
                  )),
              //Buttons
              Container(
                height: MediaQuery.of(context).size.height * 0.5,
                child: Container(
                  padding: EdgeInsets.all(40),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      //Login
                      ButtonColor(
                          onPressed: () =>
                              AppNavigator.launchLoginScreen(context),
                          child: Text(
                            StringHelper.login.toUpperCase(),
                            style: TextStyle(
                                color: ColorsHelper.whiteColor(), fontSize: 15),
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      //register
                      ButtonColor(
                          onPressed: () =>
                              AppNavigator.launchRegisterScreen(context),
                          child: Text(
                            StringHelper.register.toUpperCase(),
                            style: TextStyle(
                                color: ColorsHelper.whiteColor(), fontSize: 15),
                          )),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
}
