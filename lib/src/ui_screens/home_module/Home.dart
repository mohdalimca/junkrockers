import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:junkrocker/src/bloc/bloc_home/home_bloc.dart';
import 'package:junkrocker/src/bloc/bloc_main/main_bloc.dart';
import 'package:junkrocker/src/model/BookingDetailModel.dart';
import 'package:junkrocker/src/model/UpcomingBookingModel.dart';
import 'package:junkrocker/src/model/ZipCodeModel.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/ui_screens/home_module/booking_popup.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';

class Home extends StatefulWidget {
  static const String routeName = "/Home";
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var exitApp;
  String userProfilePic = "";
  TextEditingController zipCodeController = TextEditingController();

  FocusNode zipCodeNode = FocusNode();

  double width = 1080, height = 1920;

  HomeBloc homeBloc = HomeBloc();
  MainBloc mainBloc;

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    exitApp = 0;
    super.initState();
    callUpcommingBookings();
    callBookingQuc();
  }

  // callData() async {
  //   userProfilePic = await SharePreferencesHelper.getInstant()
  //           .getString(SharePreferencesHelper.Profile_Pic) ??
  //       "";
  //   if (mounted) {
  //     setState(() {});
  //   }
  // }

  callBookingQuc() async {
    print(mainBloc.bookingQueList.length);
    await mainBloc.callBookingQuesApi(context: context);
    await mainBloc.callstep5DataApi(context: context);
    print(mainBloc.bookingQueList.length);
  }

  List<UpcomingBookingData> upcomingBooking = [];

  callUpcommingBookings() async {
    upcomingBooking = await homeBloc.callUpcomingBookingsApi(context: context);
    if (mounted) {
      setState(() {});
    }
  }

  Future<bool> onWillPop() {
    Fluttertoast.showToast(
        msg: StringHelper.press_back_again_to_exit,
        backgroundColor: ColorsHelper.blueButtonColor(),
        textColor: Colors.white);
    exitApp++;
    Future.delayed(Duration(milliseconds: 1000), () {
      if (exitApp >= 2) {
        exit(0);
      } else {
        exitApp = 0;
      }
    });
    return Future.value(false);
  }

  checkZipCode() async {
    List<ZipCodeData> zipCodeDataList = await homeBloc.callZipCodeApi(
        zipcode: zipCodeController.text, context: context);
    if (zipCodeDataList != null) {
      BookingPopup.showBookingPopup(
          contextHome: context, zipCode: zipCodeDataList[0].zipcode);
    } else {
      // PopupDialogs.displayMessage(
      //     context, StringHelper.service_not_availble_at_this_ZipCode);
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return WillPopScope(
      onWillPop: onWillPop,
      child: Container(
        color: ColorsHelper.backgroundColor(),
        child: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: ScreenUtil().setHeight(
                height * 0.40,
              ),
              child: Image.asset(
                ImageAssets.img_home_bg,
                fit: BoxFit.fill,
              ),
            ),
            GestureDetector(
              onTap: () => callUnfocus(context: context),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                appBar: appBarBody(
                    title: StringHelper.app_name,
                    textColor: ColorsHelper.whiteColor(),
                    leadingIcon: ImageAssets.ic_user,
                    onTapLeading: () =>
                        AppNavigator.launchProfileScreen(context),
                    action1Icon: ImageAssets.ic_bell,
                    onTapaction1: () =>
                        AppNavigator.launchNotificationsScreen(context)),
                body: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: homeBody(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget homeBody() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: ScreenUtil().setHeight(height * 0.01),
          ),
          homeUpperBody(),
          SizedBox(
            height: ScreenUtil().setHeight(height * 0.12),
          ),
          homeMiddleBody(),
          SizedBox(
            height: ScreenUtil().setHeight(height * 0.05),
          ),
          //upcoming_Booking
          Text(
            StringHelper.upcoming_Booking,
            textAlign: TextAlign.start,
            style:
                TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 15),
          ),
          SizedBox(
            height: 20,
          ),
          Flexible(child: homeLowwerBody())
        ],
      );

  Widget homeUpperBody() => Column(
        children: <Widget>[
          //no_Hassle_Up_Fornt_Online
          Text(
            StringHelper.no_Hassle_Up_Fornt_Online,
            style: TextStyle(
              color: ColorsHelper.whiteColor(),
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 10,
          ),
          //Enter ZipCode and check
          Container(
            decoration: BoxDecoration(
              color: ColorsHelper.whiteColor(),
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
            child: Row(
              children: <Widget>[
                //ZipCode
                Flexible(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: TextFormField(
                      textAlign: TextAlign.start,
                      maxLines: 1,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(6),
                      ],
                      controller: zipCodeController,
                      keyboardType: TextInputType.number,
                      focusNode: zipCodeNode,
                      textInputAction: TextInputAction.done,
                      onFieldSubmitted: (str) =>
                          FocusScope.of(context).requestFocus(new FocusNode()),
                      decoration: InputDecoration(
                        hintText: StringHelper.enter_zipCode,
                        hintStyle:
                            TextStyle(color: ColorsHelper.greyTextBoxColor()),
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                //Check
                Material(
                  child: InkWell(
                    borderRadius: BorderRadius.circular(10),
                    onTap: () {
                      callUnfocus(context: context);
                      if (zipCodeController.text != '') {
                        checkZipCode();
                      } else {
                        PopupDialogs.displayMessage(
                            context, StringHelper.please_enter_ZipCode);
                      }
                    },
                    child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Text(
                          StringHelper.check,
                          style:
                              TextStyle(color: ColorsHelper.redishTextColor()),
                        )),
                  ),
                ),
                SizedBox(
                  width: 10,
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          //no_credit_card_required
          Text(
            StringHelper.no_credit_card_required,
            style: TextStyle(
              color: ColorsHelper.whiteColor(),
            ),
            textAlign: TextAlign.center,
          ),
        ],
      );

  Widget homeMiddleBody() => Row(
        children: <Widget>[
          Container(
            height: ScreenUtil().setWidth(height * 0.2),
            // MediaQuery.of(context).size.width * 0.35,
            width: ScreenUtil().setWidth(height * 0.2),
            //MediaQuery.of(context).size.width * 0.35,
            child: Image.asset(
              ImageAssets.img_customer_service,
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //customer_Services_for_hassle_free_Experience
                Text(
                  StringHelper.customer_Services_for_hassle_free_Experience,
                  style: TextStyle(color: ColorsHelper.greySubHeadColor()),
                ),
                SizedBox(
                  height: 5,
                ),
                //book_on_call_now
                Row(
                  children: <Widget>[
                    Text(
                      StringHelper.book_on_call_now,
                      style: TextStyle(color: ColorsHelper.blueButtonColor()),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 15,
                      width: 20,
                      child: Image.asset(
                        ImageAssets.ic_next,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      );

  Widget homeLowwerBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              upcomingBooking != null
                  ? bookingList()
                  :
                  //img_no_booking
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.center,
                          height: ScreenUtil().setWidth(height * 0.225),
                          //MediaQuery.of(context).size.width * 0.5,
                          width: ScreenUtil().setWidth(height * 0.225),
                          // MediaQuery.of(context).size.width * 0.5,
                          child: Image.asset(
                            ImageAssets.img_no_booking,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
              SizedBox(
                height: 10,
              ),
              //no_upcoming_Booking
              if (upcomingBooking == null)
                Center(
                  child: Text(
                    StringHelper.no_upcoming_Booking,
                    style: TextStyle(
                      color: ColorsHelper.greySubHeadColor(),
                    ),
                  ),
                ),
            ],
          ),
        ),
      );

  Widget bookingList() => Container(
        child: ListView.builder(
            padding: EdgeInsets.all(0),
            itemCount: upcomingBooking.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return Column(
                children: <Widget>[
                  InkWell(
                    borderRadius: BorderRadius.circular(7),
                    onTap: () async {
                      List<BookingDetailData> bookingDetailData = [];
                      bookingDetailData = await homeBloc.callBookingDetailApi(
                          bookingId:
                              int.tryParse(upcomingBooking[index].bookingId),
                          context: context);
                      AppNavigator.launchUpcomingBookingsScreen(
                          context, bookingDetailData[0]);
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Column(
                        children: <Widget>[
                          //itemTypeProduct bookingDate
                          rowHelper(
                            child1: Text(
                              upcomingBooking[index].itemTypeProduct,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: ColorsHelper.blueButtonColor()),
                            ),
                            child2: Text(
                              upcomingBooking[index].bookingDate.split(" ")[0],
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: ColorsHelper.greySubHeadColor()),
                            ),
                          ),
                          //bookingType
                          rowHelper(
                              child1: Text(
                                upcomingBooking[index].bookingType,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: ColorsHelper.greySubHeadColor()),
                              ),
                              child2: Container(
                                height: 0,
                                width: 0,
                              )),
                          //price
                          rowHelper(
                            child1: Text(
                              " ",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: ColorsHelper.blueButtonColor()),
                            ),
                            child2: Text(
                              "\$ ${upcomingBooking[index].price}",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: ColorsHelper.greenDollerColor()),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Divider(
                    height: 0,
                    thickness: 2,
                  )
                ],
              );
            }),
      );

  Widget rowHelper({Widget child1, Widget child2}) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[child1, child2],
      );
}
