import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junkrocker/src/bloc/bloc_main/main_bloc.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';

class BookingPopup {
  static showBookingPopup({BuildContext contextHome, String zipCode}) {
    return showDialog(
        context: contextHome,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () => null,
            child: Scaffold(
              backgroundColor: Colors.transparent,
              body: Center(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  margin: EdgeInsets.all(20),
                  // height: MediaQuery.of(context).size.height * 0.42,
                  decoration: BoxDecoration(
                      color: ColorsHelper.whiteColor(),
                      border: Border.all(),
                      borderRadius: BorderRadius.circular(10)),
                  child: bookingPopupBody(
                      context: context,
                      contextHome: contextHome,
                      zipCode: zipCode),
                ),
              ),
            ),
          );
        });
  }

  static Widget bookingPopupBody(
      {BuildContext contextHome, BuildContext context, String zipCode}) {
    MainBloc mainBloc;
    mainBloc = BlocProvider.of<MainBloc>(contextHome);
    print(mainBloc.name);
    return ScrollConfiguration(
      behavior: ListViewScrollBehavior(),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                InkWell(
                  onTap: () => AppNavigator.popBackStack(context),
                  child: Icon(
                    Icons.cancel,
                    color: ColorsHelper.blueButtonColor(),
                  ),
                )
              ],
            ),
            //book_Online
            Container(
              child: Text(
                StringHelper.book_Online,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: ColorsHelper.blueButtonColor(), fontSize: 15),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            //Service available at
            RichText(
                text: TextSpan(
                    style: TextStyle(
                        color: ColorsHelper.greySubHeadColor(), fontSize: 15),
                    children: [
                  TextSpan(text: StringHelper.service),
                  TextSpan(
                      text: StringHelper.available,
                      style: TextStyle(
                        color: ColorsHelper.greenDollerColor(),
                      )),
                  TextSpan(text: StringHelper.at),
                ])),
            SizedBox(
              height: 10,
            ),
            //Zipcode and edit
            Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                Text(
                  zipCode,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: ColorsHelper.greySubHeadColor(), fontSize: 30),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Material(
                      child: InkWell(
                        onTap: () => AppNavigator.popBackStack(context),
                        child: Container(
                            alignment: Alignment.centerRight,
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            child: Text(
                              StringHelper.edit,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: ColorsHelper.redishTextColor(),
                                  fontSize: 15),
                            )),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            //Buttons
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  //commercial
                  ButtonColor(
                      onPressed: () {
                        mainBloc.zipcode = int.tryParse(zipCode);
                        mainBloc.bookingType = StringHelper.commercial;
                        AppNavigator.launchStep2BookingScreen(context);
                      },
                      child: Text(
                        StringHelper.commercial.toUpperCase(),
                        style: TextStyle(
                            color: ColorsHelper.whiteColor(), fontSize: 15),
                      )),
                  SizedBox(
                    height: 15,
                  ),
                  //residentail
                  ButtonColor(
                      onPressed: () {
                        mainBloc.zipcode = int.tryParse(zipCode);
                        mainBloc.bookingType = StringHelper.residentail;
                        AppNavigator.launchStep2BookingScreen(context);
                      },
                      child: Text(
                        StringHelper.residentail.toUpperCase(),
                        style: TextStyle(
                            color: ColorsHelper.whiteColor(), fontSize: 15),
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
