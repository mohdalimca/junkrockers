import 'package:flutter/material.dart';
import 'package:junkrocker/src/bloc/bloc_profile/profile_bloc.dart';
import 'package:junkrocker/src/data/repository/profile_repository.dart';
import 'package:junkrocker/src/model/BookingHistoryModel.dart';
import 'package:junkrocker/src/model/PagesModel.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';

import 'edit_profile.dart';

class Profile extends StatefulWidget {
  static const String routeName = "/Profile";
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String name, email;
  int userId;
  String userProfilePic = "";
  ProfileBloc profileBloc = ProfileBloc();
  @override
  void initState() {
    callData();
    super.initState();
  }

  callData() async {
    name = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Full_Name);
    email = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Email);
    userId = await SharePreferencesHelper.getInstant()
        .getInt(SharePreferencesHelper.Id);
    userProfilePic = await SharePreferencesHelper.getInstant()
            .getString(SharePreferencesHelper.Profile_Pic) ??
        "";
    if (mounted) {
      setState(() {});
    }
  }

  openSlugPages({String slug}) async {
    List<PagesData> pageData =
        await profileBloc.callPagesDataApi(context: context, pageSlug: slug);
    if (pageData != null) {
      AppNavigator.launchSlugPageScreen(context, pageData[0]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsHelper.backgroundColor(),
      appBar: appBarBody(
        title: "",
        leadingIcon: null,
        onTapLeading: () => AppNavigator.popBackStack(context),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: profileBody(),
      ),
    );
  }

  Widget profileBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
            //profile
            Text(
              StringHelper.profile,
              style: TextStyle(
                  fontSize: 30, color: ColorsHelper.blueButtonColor()),
            ),
            SizedBox(
              height: 10,
            ),
            userDetail(),
            SizedBox(
              height: 20,
            ),
            //booking_history
            profileOptions(
                title: StringHelper.booking_history,
                onTap: () async {
                  List<BookingHistoryData> bookingHistoryList = [];
                  bookingHistoryList = await profileBloc.callBookingHistoryApi(
                      userId: userId, context: context);
                  AppNavigator.launchBookingHistoryScreen(
                      context, bookingHistoryList);
                }),
            //about_us
            profileOptions(
                title: StringHelper.about_us,
                onTap: () => openSlugPages(slug: StringHelper.about_us_Slug)),
            //privacy_policy
            profileOptions(
                title: StringHelper.privacy_policy,
                onTap: () =>
                    openSlugPages(slug: StringHelper.privacy_policy_Slug)),
            //terms_and_conditions
            profileOptions(
                title: StringHelper.terms_and_conditions,
                onTap: () =>
                    openSlugPages(slug: StringHelper.term_and_condition_Slug)),
            //change_Password
            profileOptions(
                title: StringHelper.change_Password,
                onTap: () => AppNavigator.launchChangePasswordScreen(context)),
            //logout
            profileOptions(
                title: StringHelper.logout,
                onTap: () async {
                  ProfileRepository.logoutApiCall(context: context);
                  var email, password, isRemember;
                  email = await SharePreferencesHelper.getInstant()
                      .getString(SharePreferencesHelper.Email);
                  isRemember = await SharePreferencesHelper.getInstant()
                      .getBool(SharePreferencesHelper.Is_Save_password);
                  if (isRemember)
                    password = await SharePreferencesHelper.getInstant()
                        .getString(SharePreferencesHelper.Password);
                  //Clear all
                  SharePreferencesHelper.getInstant().clearPreference();
                  //setting email passowrd
                  SharePreferencesHelper.getInstant()
                      .setString(SharePreferencesHelper.Email, email);
                  if (isRemember) {
                    SharePreferencesHelper.getInstant()
                        .setString(SharePreferencesHelper.Password, password);
                    SharePreferencesHelper.getInstant().setBool(
                        SharePreferencesHelper.Is_Save_password, isRemember);
                  }
                  SharePreferencesHelper.getInstant()
                      .setBool(SharePreferencesHelper.Is_Login, false);
                  AppNavigator.launchWelcomeScreen(context);
                },
                isDivider: false),
          ]),
        ),
      );

  Widget userDetail() => Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          //ic_profile02
          userProfilePic == ""
              ? Container(
                  height: 50,
                  width: 50,
                  child: Image.asset(
                    ImageAssets.ic_profile02,
                    fit: BoxFit.fill,
                  ),
                )
              : Card(
                  margin: EdgeInsets.all(0),
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(1000.0)),
                  child: Container(
                    height: 50,
                    width: 50,
                    child: Image.network(
                      userProfilePic,
                      fit: BoxFit.cover,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.all(Radius.circular(1000.0)),
                    ),
                  ),
                ),
          SizedBox(
            width: 10,
          ),
          //name & email
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //name
                Text(
                  name ?? StringHelper.name,
                  style: TextStyle(color: ColorsHelper.blueButtonColor()),
                ),
                //Email
                Text(
                  email ?? StringHelper.email,
                  style: TextStyle(color: ColorsHelper.greySubHeadColor()),
                ),
              ],
            ),
          ),
          //Edit button
          Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () => Navigator.pushNamed(
                context,
                EditProfile.routeName,
              ).then((value) => callData()),
              child: Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Text(
                    StringHelper.edit,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: ColorsHelper.redishTextColor(), fontSize: 15),
                  )),
            ),
          ),
        ],
      );

  Widget profileOptions(
          {String title, Function onTap, bool isDivider = true}) =>
      Column(
        children: <Widget>[
          InkWell(
            borderRadius: BorderRadius.circular(7),
            onTap: onTap,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  //title
                  Text(
                    title,
                    style: TextStyle(color: ColorsHelper.blueButtonColor()),
                  ),
                  //ic_menu_next
                  Container(
                    height: 20,
                    width: 10,
                    child: Image.asset(
                      ImageAssets.ic_menu_next,
                      fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (isDivider)
            Divider(
              thickness: 2,
            )
        ],
      );
}
