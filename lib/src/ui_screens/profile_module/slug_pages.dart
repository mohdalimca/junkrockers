import 'package:flutter/material.dart';
import 'package:junkrocker/src/model/PagesModel.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';

class SlugPage extends StatefulWidget {
  static const String routeName = "/SlugPage";
  final PagesData pageData;
  SlugPage({Key key, this.pageData}) : super(key: key);

  @override
  _SlugPageState createState() => _SlugPageState();
}

class _SlugPageState extends State<SlugPage> {
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
          title: "",
          leadingIcon: null,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: slugPageBody(),
        ),
      ),
    );
  }

  Widget slugPageBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //pageTitle
              textHelper(
                  title: widget.pageData.pageTitle,
                  textColor: ColorsHelper.blueButtonColor(),
                  fontSize: 30),
              SizedBox(
                height: 20,
              ),
              assetImageHelper(image: ImageAssets.logo_aboutus, height: 150),
              SizedBox(
                height: 20,
              ),
              textHelper(
                  title: widget.pageData.pageContent,
                  textColor: ColorsHelper.blueButtonColor())
            ],
          ),
        ),
      );
}
