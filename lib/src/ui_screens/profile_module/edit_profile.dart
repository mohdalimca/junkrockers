import 'dart:io';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/bloc/bloc_profile/profile_bloc.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:junkrocker/src/utils/validator/validator.dart';

class EditProfile extends StatefulWidget {
  static const String routeName = "/EditProfile";
  EditProfile({Key key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  int id;
  File profilePic;
  String userProfilePic = "";
  final formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();

  FocusNode nameNode = FocusNode();

  ProfileBloc profileBloc = ProfileBloc();

  @override
  void initState() {
    callData();
    super.initState();
  }

  callData() async {
    nameController.text = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Full_Name);
    id = await SharePreferencesHelper.getInstant()
        .getInt(SharePreferencesHelper.Id);
    userProfilePic = await SharePreferencesHelper.getInstant()
            .getString(SharePreferencesHelper.Profile_Pic) ??
        "";
    // email = await SharePreferencesHelper.getInstant()
    //     .getString(SharePreferencesHelper.Email);
    if (mounted) {
      setState(() {});
    }
  }

  submit() async {
    var result = await profileBloc.callUpdateProfileApi(
        id: id,
        fullName: nameController.text.trim(),
        profileImage: profilePic,
        context: context);
    if (result != null) {
      AppNavigator.popBackStack(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
          title: "",
          leadingIcon: null,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: editProfileBody(),
        ),
      ),
    );
  }

  Widget editProfileBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //edit_profile
              Text(
                StringHelper.edit_profile,
                style: TextStyle(
                    fontSize: 30, color: ColorsHelper.blueButtonColor()),
              ),
              SizedBox(
                height: 20,
              ),
              //ic_profile02
              Center(
                  child: userProfilePic == ""
                      ? uploadPicBody()
                      : Card(
                          margin: EdgeInsets.all(0),
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(1000.0)),
                          child: Container(
                            height: MediaQuery.of(context).size.width * 0.35,
                            width: MediaQuery.of(context).size.width * 0.35,
                            child: Image.network(
                              userProfilePic,
                              fit: BoxFit.cover,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(1000.0)),
                            ),
                          ),
                        )),
              SizedBox(
                height: 10,
              ),
              //update_Image button
              Center(
                child: InkWell(
                  borderRadius: BorderRadius.circular(10),
                  onTap: () {
                    userProfilePic = "";
                    setState(() {});
                    profileBloc.imagePicker(context);
                  },
                  child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Text(
                        StringHelper.update_Image,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: ColorsHelper.redishTextColor(),
                            fontSize: 18),
                      )),
                ),
              ),
              formBody(),
              SizedBox(
                height: 20,
              ),
              saveButton()
            ],
          ),
        ),
      );

  Widget formBody() => Form(
      key: formKey,
      autovalidate: profileBloc.autoValidation,
      child: Column(
        children: <Widget>[
          //Email
          signUpformFeild(
              tilte: StringHelper.name,
              validation: CommonValidator.emptyValidation,
              controller: nameController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: nameNode,
              nextFocusNode: null,
              textInputAction: TextInputAction.done,
              context: context),
          SizedBox(
            height: 10,
          ),
        ],
      ));

  //saveButton
  Widget saveButton() => ButtonColor(
      onPressed: () {
        callUnfocus(context: context);
        profileBloc.onLoginClick(formKey).then((onValue) {
          if (onValue) {
            submit();
          } else {
            if (mounted) setState(() {});
          }
        });
      },
      child: Text(
        StringHelper.save.toUpperCase(),
        style: TextStyle(color: ColorsHelper.whiteColor(), fontSize: 15),
      ));

  Widget uploadPicBody() => StreamBuilder(
      stream: profileBloc.image,
      builder: (context, snapshot) {
        if (snapshot.data != null) {
          profilePic = snapshot.data;
          return Card(
            margin: EdgeInsets.all(0),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1000.0)),
            child: Container(
              height: MediaQuery.of(context).size.width * 0.35,
              width: MediaQuery.of(context).size.width * 0.35,
              child: Image.file(
                snapshot.data,
                fit: BoxFit.cover,
              ),
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.all(Radius.circular(1000.0)),
              ),
            ),
          );
        } else {
          return Image.asset(
            ImageAssets.ic_profile02,
            fit: BoxFit.fill,
            height: MediaQuery.of(context).size.width * 0.35,
            width: MediaQuery.of(context).size.width * 0.35,
          );
        }
      });
}
