import 'package:flutter/material.dart';
import 'package:junkrocker/src/bloc/bloc_profile/profile_bloc.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:junkrocker/src/utils/validator/validator.dart';

class ChangePassword extends StatefulWidget {
  static const String routeName = "/ChangePassword";
  ChangePassword({Key key}) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  bool isHideP = true, isHideP1 = true, isHideP2 = true;
  final formKey = GlobalKey<FormState>();

  TextEditingController oldPwdController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPwdController = TextEditingController();

  FocusNode oldPwdNode = FocusNode();
  FocusNode passwordNode = FocusNode();
  FocusNode confirmPwdNode = FocusNode();

  ProfileBloc profileBloc = ProfileBloc();

  submit() async {
    int id = await SharePreferencesHelper.getInstant()
        .getInt(SharePreferencesHelper.Id);
    var result = await profileBloc.callChangePasswordApi(
        id: id,
        oldPwd: oldPwdController.text.trim(),
        newPwd: passwordController.text.trim(),
        cNewPwd: confirmPwdController.text.trim(),
        context: context);
    if (result != null) {
      var email;
      email = await SharePreferencesHelper.getInstant()
          .getString(SharePreferencesHelper.Email);
      //Clear all
      SharePreferencesHelper.getInstant().clearPreference();
      //setting email passowrd
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Email, email);
      SharePreferencesHelper.getInstant()
          .setBool(SharePreferencesHelper.Is_Login, false);
      AppNavigator.launchWelcomeScreen(context);
      PopupDialogs.displayMessageOnly(
          context, StringHelper.please_login_using_your_new_credential);
    }
  }

  @override
  void dispose() {
    profileBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
          title: "",
          leadingIcon: null,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: changePasswordBody(),
        ),
      ),
    );
  }

  Widget changePasswordBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //change_Password
              Text(
                StringHelper.change_Password,
                style: TextStyle(
                    fontSize: 30, color: ColorsHelper.blueButtonColor()),
              ),
              SizedBox(
                height: 40,
              ),
              formBody(),
              SizedBox(
                height: 40,
              ),
              //loginButton
              changePasswordButton(),
            ],
          ),
        ),
      );

  Widget formBody() => Form(
      key: formKey,
      autovalidate: profileBloc.autoValidation,
      child: Column(
        children: <Widget>[
          //OldPassword
          signUpformFeild(
              tilte: StringHelper.old_password,
              validation: CommonValidator.passValidation,
              controller: oldPwdController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: oldPwdNode,
              nextFocusNode: passwordNode,
              textInputAction: TextInputAction.next,
              showEye: true,
              showEyeFunction: () {
                if (mounted) {
                  isHideP = !isHideP;
                  setState(() {});
                }
              },
              obscureText: isHideP,
              context: context),
          SizedBox(
            height: 10,
          ),
          //Password
          signUpformFeild(
              tilte: StringHelper.password,
              validation: CommonValidator.passValidation,
              controller: passwordController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: passwordNode,
              nextFocusNode: confirmPwdNode,
              textInputAction: TextInputAction.next,
              showEye: true,
              showEyeFunction: () {
                if (mounted) {
                  isHideP1 = !isHideP1;
                  setState(() {});
                }
              },
              obscureText: isHideP1,
              context: context),
          SizedBox(
            height: 10,
          ),
          //confirm_Password
          signUpformFeild(
              tilte: StringHelper.confirm_Password,
              validation: CommonValidator.passValidation,
              controller: confirmPwdController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: confirmPwdNode,
              nextFocusNode: null,
              textInputAction: TextInputAction.done,
              showEye: true,
              showEyeFunction: () {
                if (mounted) {
                  isHideP2 = !isHideP2;
                  setState(() {});
                }
              },
              obscureText: isHideP2,
              context: context)
        ],
      ));

  //ChangePassword
  Widget changePasswordButton() => ButtonColor(
      onPressed: () {
        callUnfocus(context: context);
        profileBloc.onLoginClick(formKey).then((onValue) {
          if (onValue) {
            if (passwordController.text == confirmPwdController.text) {
              submit();
            } else {
              PopupDialogs.displayMessage(context,
                  StringHelper.password_and_Confirm_Password_Do_not_match);
            }
          } else {
            if (mounted) setState(() {});
          }
        });
      },
      child: Text(
        StringHelper.change_Password.toUpperCase(),
        style: TextStyle(color: ColorsHelper.whiteColor(), fontSize: 15),
      ));
}
