import 'package:flutter/material.dart';
import 'package:junkrocker/src/model/BookingHistoryModel.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';

class BookingHistory extends StatefulWidget {
  static const String routeName = "/BookingHistory";
  final List<BookingHistoryData> bookingHistoryList;
  BookingHistory({Key key, @required this.bookingHistoryList})
      : super(key: key);

  @override
  _BookingHistoryState createState() => _BookingHistoryState();
}

class _BookingHistoryState extends State<BookingHistory> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
          title: "",
          leadingIcon: null,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: bookingHistoryBody(),
        ),
      ),
    );
  }

  Widget bookingHistoryBody() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          //booking_history
          Text(
            StringHelper.booking_history,
            style:
                TextStyle(fontSize: 30, color: ColorsHelper.blueButtonColor()),
          ),
          SizedBox(
            height: 10,
          ),
          //recent_bookings
          Text(
            StringHelper.recent_bookings,
            textAlign: TextAlign.start,
            style:
                TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 15),
          ),
          SizedBox(
            height: 20,
          ),
          Flexible(child: homeLowwerBody())
        ],
      );

  Widget homeLowwerBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              widget.bookingHistoryList != null
                  ? bookingList()
                  :
                  //img_no_booking
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.center,
                          height: MediaQuery.of(context).size.width * 0.5,
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: Image.asset(
                            ImageAssets.img_no_booking,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
              SizedBox(
                height: 10,
              ),
              if (widget.bookingHistoryList == null)
                //no_upcoming_Booking
                Center(
                  child: Text(
                    StringHelper.no_Bookings_History,
                    style: TextStyle(
                      color: ColorsHelper.greySubHeadColor(),
                    ),
                  ),
                ),
            ],
          ),
        ),
      );

  Widget bookingList() => Container(
        child: ListView.builder(
            padding: EdgeInsets.all(0),
            itemCount: widget.bookingHistoryList.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: <Widget>[
                        rowHelper(
                          child1: Text(
                            widget.bookingHistoryList[index].itemTypeProduct,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: ColorsHelper.blueButtonColor()),
                          ),
                          child2: Text(
                            widget.bookingHistoryList[index].bookingDate
                                .split(" ")[0],
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: ColorsHelper.greySubHeadColor()),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        rowHelper(
                            child1: Text(
                              StringHelper.transaction_number,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: ColorsHelper.greySubHeadColor()),
                            ),
                            child2: Text(
                              widget.bookingHistoryList[index].transactionNumber
                                  .split(" ")[0],
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: ColorsHelper.greySubHeadColor()),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        rowHelper(
                          child1: Text(
                            "2-seat coach, Recliner,Loveseat",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: ColorsHelper.blueButtonColor()),
                          ),
                          child2: Text(
                            "\$ ${widget.bookingHistoryList[index].price}",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: ColorsHelper.greenDollerColor()),
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(
                    height: 0,
                    thickness: 2,
                  )
                ],
              );
            }),
      );

  Widget rowHelper({Widget child1, Widget child2}) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[child1, child2],
      );
}
