import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/repository/common_repository.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'dart:async';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:package_info/package_info.dart';

class Splash extends StatefulWidget {
  static const String routeName = "/";
  Splash({Key key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  bool isLogin = false;
  // MainBloc mainBloc;

  @override
  void initState() {
    // mainBloc = BlocProvider.of<MainBloc>(context);
    // mainBloc.getIntialData(context);
    Future.delayed(Duration(milliseconds: 1000), () async {
      // bool result = await checkAppVersion();
      // if (result) launchLoginOrDashboardPage(context);
      launchLoginOrDashboardPage(context);
    });
    super.initState();
  }

  Future<bool> checkAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    print(appName);
    print(packageName);
    print(version);
    print(buildNumber);
    String result =
        await CommonRepository.checkAppVersionApiCall(context: context);
    if (result.contains(version)) {
      return true;
    } else {
      PopupDialogs.displayAppUpdate(
          context, "Please Update the app from play store");
      return false;
    }
  }

  void launchLoginOrDashboardPage(BuildContext context) async {
    isLogin = await SharePreferencesHelper.getInstant()
        .getBool(SharePreferencesHelper.Is_Login);
    if (isLogin)
      AppNavigator.launchHomeScreen(context);
    else
      AppNavigator.launchWelcomeScreen(context);
  }

  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          body: Container(
        color: ColorsHelper.backgroundColor(),
        //logo_splash
        child: Center(
            child: Image.asset(
          ImageAssets.logo_splash,
          fit: BoxFit.fill,
          height: MediaQuery.of(context).size.width * 0.8,
          width: MediaQuery.of(context).size.width * 0.8,
        )),
      )),
    );
  }
}
