import 'package:calendar_strip/calendar_strip.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junkrocker/src/bloc/bloc_booking_steps/booking_steps_bloc.dart';
import 'package:junkrocker/src/bloc/bloc_main/main_bloc.dart';
import 'package:junkrocker/src/model/SlotBookingsModel.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/buttons/button.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:junkrocker/src/utils/validator/validator.dart';
import 'package:square_in_app_payments/in_app_payments.dart';
import 'package:square_in_app_payments/models.dart';
// import 'package:stripe_payment/stripe_payment.dart';

class Step6Booking extends StatefulWidget {
  static const String routeName = "/Step6Booking";
  Step6Booking({Key key}) : super(key: key);

  @override
  _Step6State createState() => _Step6State();
}

class _Step6State extends State<Step6Booking> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now().add(Duration(days: 30));
  DateTime selectedDate = DateTime.now();
  String selectedDateText, slotTime = '', cardToken;

  BookingStepsBloc bookingStepsBloc = BookingStepsBloc();

  MainBloc mainBloc;

  List<SlotBookingsData> slotBookingsList = [];
  List<bool> slotListBool = [];

  bool isHome = true, isPickUp = false, isNotify = false, isCallApi = false;

  final formKey = GlobalKey<FormState>();

  TextEditingController flatController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController landmarkController = TextEditingController();
  TextEditingController cityController = TextEditingController();

  FocusNode flatNode = FocusNode();
  FocusNode streetNode = FocusNode();
  FocusNode landmarkNode = FocusNode();
  FocusNode cityNode = FocusNode();

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    callBoolFun();
    super.initState();
    // StripePayment.setOptions(StripeOptions(
    //     publishableKey: "pk_test_aSaULNS8cJU6Tvo20VAXy6rp",
    //     merchantId: "Test",
    //     androidPayMode: 'test'));
  }

  callBoolFun() async {
    slotBookingsList = [];
    slotBookingsList = await bookingStepsBloc.callBookingSlotsApi(
        slotDate: "2020-05-15", context: context);
    //selectedDate.toString().split(" ")[0], context: context);
    //slotListBool
    for (var i = 0; i < slotBookingsList.length; i++) {
      slotListBool.add(false);
    }
    if (mounted) {
      setState(() {});
    }
  }

  onSelect(data) async {
    print(selectedDate);
    slotBookingsList = [];
    slotBookingsList = await bookingStepsBloc.callBookingSlotsApi(
        slotDate: "2020-05-15", context: context);
    //data.toString().split(" ")[0], context: context);
    // print("Selected Date -> $data");
    mainBloc.slotDate = data.toString().split(" ")[0];
    slotListBool = [];
    for (var i = 0; i < slotBookingsList.length; i++) {
      slotListBool.add(false);
    }
    slotTime = '';
    if (mounted) {
      setState(() {});
    }
  }

  _monthNameWidget(monthName) {
    return Container(
      child: Text(monthName,
          style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w600,
              color: ColorsHelper.blueButtonColor(),
              fontStyle: FontStyle.italic)),
      padding: EdgeInsets.only(top: 8, bottom: 4),
    );
  }

  dateTileBuilder(
      date, selectedDate, rowIndex, dayName, isDateMarked, isDateOutOfRange) {
    bool isSelectedDate = date.compareTo(selectedDate) == 0;
    Color fontColor =
        isDateOutOfRange ? Colors.black26 : ColorsHelper.blueButtonColor();
    TextStyle normalStyle =
        TextStyle(fontSize: 17, fontWeight: FontWeight.w800, color: fontColor);
    TextStyle selectedStyle = TextStyle(
        fontSize: 17,
        fontWeight: FontWeight.w800,
        color: ColorsHelper.whiteColor());
    TextStyle dayNameStyle = TextStyle(
        fontSize: 12,
        color: isSelectedDate
            ? ColorsHelper.whiteColor()
            : ColorsHelper.blueButtonColor());

    List<Widget> _children = [
      Text(dayName, style: dayNameStyle),
      Text(date.day.toString(),
          style: !isSelectedDate ? normalStyle : selectedStyle),
    ];

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 8, left: 5, right: 5, bottom: 5),
      decoration: BoxDecoration(
        color: !isSelectedDate
            ? Colors.transparent
            : ColorsHelper.redishTextColor(),
        borderRadius: BorderRadius.all(Radius.circular(60)),
      ),
      child: Column(
        children: _children,
      ),
    );
  }

  @override
  void dispose() {
    bookingStepsBloc.close();
    super.dispose();
  }

  submit() async {
    mainBloc.slotTime = slotTime;
    mainBloc.address = flatController.text.trim() +
        " " +
        streetController.text.trim() +
        " " +
        landmarkController.text.trim() +
        " " +
        cityController.text.trim() +
        " ";
    mainBloc.states = cityController.text.trim();
    mainBloc.phoneNumber = 7869720025;
    mainBloc.isPickupAddress = isPickUp ? 1 : 0;
    mainBloc.isNotify = isNotify ? 1 : 0;
    mainBloc.addressType = isHome ? StringHelper.home : StringHelper.business;
    var token = await _pay();
    // mainBloc.cardToken = cardToken;
    // //   await paymentFunction();
    // var result;
    // if (token && isCallApi) result = await mainBloc.callDoBookingApi(context);
    // if (result != null) {
    //   AppNavigator.launchHomeScreen(context);
    //   PopupDialogs.displayMessageOnly(context, result);
    // }
  }

  // Future<bool> paymentFunction() async {
  //   // PaymentMethod paymentMethod =
  //   //     await StripePayment.paymentRequestWithCardForm(
  //   //         CardFormPaymentRequest());
  //   // Token token = await bookingStepsBloc.addCard(context);
  //   // if (token == null) {
  //   //   _scaffoldKey.currentState
  //   //       .showSnackBar(SnackBar(content: Text("Unable to Proceed Payment")));
  //   //   return false;
  //   // } else {
  //   //   _scaffoldKey.currentState.showSnackBar(
  //   //       SnackBar(content: Text("subscribing using ${token.tokenId}")));
  //   //   return true;
  //   // }
  // }

  // void setError(dynamic error) {
  //   _scaffoldKey.currentState
  //       .showSnackBar(SnackBar(content: Text(error.toString())));
  //   setState(() {
  //     // _error = error.toString();
  //   });
  // }

  Future<bool> _pay() async {
    InAppPayments.setSquareApplicationId(
        "sandbox-sq0idb-eCufoD5Hu7cACvNqsRTa1A");
    InAppPayments.startCardEntryFlow(
        onCardEntryCancel: _onCardEntryCancel,
        onCardNonceRequestSuccess: _onCardNonceRequestSuccess,
        collectPostalCode: false);
    return true;
  }

  void _onCardEntryCancel() {}

  Future<void> _onCardNonceRequestSuccess(CardDetails cardDetails) async {
    cardToken = cardDetails.nonce;
    if (mounted) {
      setState(() {
        isCallApi = true;
      });
      mainBloc.cardToken ="cnon:card-nonce-ok";
         //"EAAAECDP4L6YSAuVOSTE9P7YZyedymbebAY5tgF6AFT0g4aR06G_MQA9HghZBcJO";
      //cardToken.split(":").last;
      //   await paymentFunction();
      var result;
      if (isCallApi) result = await mainBloc.callDoBookingApi(context);
      if (result != null) {
        print(cardDetails.nonce);
        InAppPayments.completeCardEntry(
            onCardEntryComplete: _onCardEntryComplete);
        AppNavigator.launchHomeScreen(context);
        PopupDialogs.displayMessageOnly(context, result);
      }
    }
  }

  void _onCardEntryComplete() {}

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
            title: StringHelper.step6,
            textColor: ColorsHelper.blueButtonColor(),
            leadingIcon: null,
            onTapLeading: () => AppNavigator.popBackStack(context),
            action1Icon: ImageAssets.ic_info,
            onTapaction1: () {}),
        body: Container(
          padding: EdgeInsets.all(20),
          child: stepBody(),
        ),
      ),
    );
  }

  Widget stepBody() => ScrollConfiguration(
      behavior: ListViewScrollBehavior(),
      child: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          //select_slot
          Text(
            StringHelper.select_slot,
            style:
                TextStyle(fontSize: 30, color: ColorsHelper.blueButtonColor()),
          ),
          SizedBox(
            height: 20,
          ),
          //date_Time
          Text(
            StringHelper.date_Time,
            textAlign: TextAlign.start,
            style:
                TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 15),
          ),
          SizedBox(
            height: 20,
          ),
          //Calender
          Container(
            child: CalendarStrip(
              addSwipeGesture: true,
              startDate: startDate,
              endDate: endDate,
              onDateSelected: onSelect,
              dateTileBuilder: dateTileBuilder,
              iconColor: ColorsHelper.blueButtonColor(),
              monthNameWidget: _monthNameWidget,
              containerHeight: 120,
              containerDecoration: BoxDecoration(
                color: ColorsHelper.whiteColor(),
                borderRadius: BorderRadius.circular(7),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          //Slots Elemnts
          if (slotBookingsList.length != 0)
            Column(
              children: <Widget>[
                slotsElements(),
                SizedBox(
                  height: 20,
                ),
                Text(
                  slotTime,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      color: ColorsHelper.blueButtonColor(), fontSize: 20),
                ),
              ],
            ),
          SizedBox(
            height: 20,
          ),
          //date_Time
          Text(
            StringHelper.address_details,
            textAlign: TextAlign.start,
            style:
                TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 15),
          ),
          SizedBox(
            height: 20,
          ),
          //Home or business
          Row(
            children: <Widget>[
              radioOption(text: StringHelper.home, bol: isHome, index: 0),
              SizedBox(
                width: 10,
              ),
              radioOption(text: StringHelper.business, bol: !isHome, index: 0)
            ],
          ),
          SizedBox(
            height: 20,
          ),
          formBody(),
          SizedBox(
            height: 20,
          ),
          //Pick Up
          radioOption(
              text: StringHelper.i_will_be_at_the_pick_up_address,
              bol: isPickUp,
              index: 1),
          SizedBox(
            height: 10,
          ),
          //notify
          radioOption(
              text: StringHelper.please_notify_me_about_promotions,
              bol: isNotify,
              index: 2),
          SizedBox(
            height: 20,
          ),
          payButton()
        ],
      )));

  Widget slotsElements() => Container(
        height: 60,
        decoration: BoxDecoration(
          color: ColorsHelper.whiteColor(),
          borderRadius: BorderRadius.circular(7),
        ),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: ListView.builder(
            padding: EdgeInsets.all(0),
            shrinkWrap: true,
            itemCount: slotBookingsList.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (contet, index) {
              return Container(
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  child: expandedElementsHelpers(
                    bol: slotListBool[index],
                    text: slotBookingsList[index].startTime,
                    horizontal: 20,
                    boolindex: index,
                  ));
            }),
      );

  Widget expandedElementsHelpers({
    bool bol,
    String text,
    double horizontal = 10,
    double vertical = 10,
    double margin = 0,
    int boolindex,
  }) =>
      Material(
        color: bol ? ColorsHelper.redishTextColor() : ColorsHelper.whiteColor(),
        borderRadius: BorderRadius.circular(7),
        child: InkWell(
          onTap: () {
            slotTime = text;
            for (var i = 0; i < slotListBool.length; i++) {
              if (i == boolindex) {
                slotListBool[boolindex] = !slotListBool[boolindex];
              } else {
                slotListBool[i] = false;
              }
            }
            if (mounted) {
              setState(() {});
            }
          },
          borderRadius: BorderRadius.circular(7),
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: horizontal, vertical: vertical),
            child: textHelper(
              fontSize: 13,
              title: text,
              textColor: bol
                  ? ColorsHelper.whiteColor()
                  : ColorsHelper.darkBlueColor(),
            ),
          ),
        ),
      );

  Widget radioOption({String text, bool bol, int index}) => Material(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(7),
        child: InkWell(
          onTap: () {
            switch (index) {
              case 0:
                isHome = !isHome;
                break;
              case 1:
                isPickUp = !isPickUp;
                break;
              case 2:
                isNotify = !isNotify;
                break;
              default:
            }

            if (mounted) {
              setState(() {});
            }
          },
          borderRadius: BorderRadius.circular(7),
          child: Container(
            padding: EdgeInsets.all(12.5),
            child: Row(
              children: <Widget>[
                //CheckBox
                assetImageHelper(
                    height: 25,
                    width: 25,
                    image: bol
                        ? ImageAssets.ic_selected
                        : ImageAssets.ic_unselected02),
                SizedBox(
                  width: 20,
                ),
                textHelper(
                  title: text,
                  textColor: ColorsHelper.darkBlueColor(),
                )
              ],
            ),
          ),
        ),
      );

  Widget formBody() => Form(
      key: formKey,
      autovalidate: bookingStepsBloc.autoValidation,
      child: Column(
        children: <Widget>[
          //flat_Floor_Appartment
          signUpformFeild(
              tilte: StringHelper.flat_Floor_Appartment,
              validation: CommonValidator.emptyValidation,
              controller: flatController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: flatNode,
              nextFocusNode: streetNode,
              textInputAction: TextInputAction.next,
              context: context),
          SizedBox(
            height: 10,
          ),
          //street
          signUpformFeild(
              tilte: StringHelper.street,
              validation: CommonValidator.emptyValidation,
              controller: streetController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: streetNode,
              nextFocusNode: landmarkNode,
              textInputAction: TextInputAction.next,
              context: context),
          SizedBox(
            height: 10,
          ),
          //landmark
          signUpformFeild(
              tilte: StringHelper.landmark,
              validation: CommonValidator.emptyValidation,
              controller: landmarkController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: landmarkNode,
              nextFocusNode: cityNode,
              textInputAction: TextInputAction.next,
              context: context),
          SizedBox(
            height: 10,
          ),
          //city_State_Pincode
          signUpformFeild(
              tilte: StringHelper.city_State_Pincode,
              validation: CommonValidator.emptyValidation,
              controller: cityController,
              textInputType: TextInputType.text,
              maxLine: 1,
              focusNode: cityNode,
              nextFocusNode: null,
              textInputAction: TextInputAction.done,
              context: context)
        ],
      ));

  //Pay
  Widget payButton() => ButtonColor(
      onPressed: () {
        bookingStepsBloc.onLoginClick(formKey).then((onValue) {
          if (onValue) {
            if (slotTime != '') {
              submit();
            } else {
              PopupDialogs.displayMessageOnly(
                  context, StringHelper.please_Select_a_Time_Slot);
            }
          } else {}
          if (mounted) setState(() {});
        });
      },
      child: Text(
        StringHelper.pay.toUpperCase(),
        style: TextStyle(color: ColorsHelper.whiteColor(), fontSize: 15),
      ));
}
