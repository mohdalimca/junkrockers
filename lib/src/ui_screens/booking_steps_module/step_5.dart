import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junkrocker/src/bloc/bloc_main/main_bloc.dart';
import 'package:junkrocker/src/model/BookingQueModel.dart';
import 'package:junkrocker/src/model/Step5Model.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';

class Step5Booking extends StatefulWidget {
  static const String routeName = "/Step5Booking";
  Step5Booking({Key key}) : super(key: key);

  @override
  _Step5State createState() => _Step5State();
}

class _Step5State extends State<Step5Booking> {
  List<bool> isOpton = [];
  bool isExpand = false;
  double totalPriceItem = 0, totalPriceTruck = 0;
  String truckLoad = '';
  String truckLoadImage =
      "http://ec2-3-6-87-120.ap-south-1.compute.amazonaws.com/assets/img/truck1.png";

  List<BookingQueData> bookingQueList = [];
  BookingQueData bookingQueData;
  List<Step5Data> step5List = [];
  List<Step5Data> itemList = [];
  List<Step5Data> truckList = [];

  List<bool> itemListBool = [];
  List<bool> truckListBool = [];

  List<bool> step5ListBool = [];
  List<double> step5ListPrice = [];
  MainBloc mainBloc;
  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    bookingQueList = mainBloc.bookingQueList;
    step5List = mainBloc.step5List;
    itemList = mainBloc.itemList;
    truckList = mainBloc.truckList;
    callBolfunction();
    super.initState();
  }

  callBolfunction() {
    bookingQueData = bookingQueList.firstWhere((element) {
      return element.step == 5;
    });
    for (var i = 0; i < 2; i++) {
      isOpton.add(false);
    }
    //itemListBool
    for (var i = 0; i < itemList.length; i++) {
      itemListBool.add(false);
      //Set All bool in itemList
      for (var j = 0; j < itemList[i].images.length; j++) {
        step5ListBool.add(false);
        step5ListPrice.add(double.tryParse(itemList[i].images[j].price));
      }
    }
    //truckListBool
    for (var i = 0; i < truckList.length; i++) {
      truckListBool.add(false);
    }
    if (mounted) {
      setState(() {});
    }
  }

  submit() {
    int index;
    for (var i = 0; i < isOpton.length; i++) {
      if (isOpton[i]) {
        index = i;
      }
    }
    if (index != null) {
      mainBloc.price = index == 0 ? totalPriceItem : totalPriceTruck;
      mainBloc.itemType = index == 0 ? 1 : 2;
      AppNavigator.launchStep6BookingScreen(context);
    } else {
      PopupDialogs.displayMessageOnly(context, StringHelper.select_any_one);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
            title: StringHelper.step5,
            textColor: ColorsHelper.blueButtonColor(),
            leadingIcon: null,
            onTapLeading: () => AppNavigator.popBackStack(context),
            action1Icon: ImageAssets.ic_info,
            onTapaction1: () {}),
        body: Container(
          padding: EdgeInsets.all(20),
          child: stepBody(),
        ),
      ),
    );
  }

  Widget stepBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // //how_much_junk_do_you_think_needs_to_be_removed
              // Text(
              //   StringHelper.how_much_junk_do_you_think_needs_to_be_removed,
              //   style: TextStyle(
              //       fontSize: 30, color: ColorsHelper.blueButtonColor()),
              // ),
              Text(
                bookingQueData.question,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: ColorsHelper.blueButtonColor(), fontSize: 20),
              ),
              SizedBox(
                height: 20,
              ),
              //price_estimator
              Text(
                StringHelper.price_estimator,
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: ColorsHelper.blueButtonColor(), fontSize: 15),
              ),
              SizedBox(
                height: 20,
              ),
              //item or truck
              Row(
                children: <Widget>[
                  optionContainer(
                      bol: isOpton[0],
                      index: 0,
                      text: StringHelper.by_list_item),
                  SizedBox(
                    width: 10,
                  ),
                  optionContainer(
                      bol: isOpton[1],
                      index: 1,
                      text: StringHelper.by_truck_load),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              if (isOpton[0])
                //Item list
                Column(
                  children: <Widget>[
                    itemListBody(),
                    //Price
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          textHelper(
                              title: StringHelper.estimated_price,
                              textColor: ColorsHelper.darkBlueColor(),
                              fontSize: 13),
                          textHelper(
                              title: "\$$totalPriceItem",
                              textColor: ColorsHelper.redishTextColor(),
                              fontSize: 18)
                        ],
                      ),
                    ),
                  ],
                ),
              if (isOpton[1])
                //truck list
                Column(
                  children: <Widget>[
                    truckLoadElements(),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      truckLoad,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: ColorsHelper.blueButtonColor(), fontSize: 20),
                    ),
                    //Price
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: textHelper(
                          title: "\$$totalPriceTruck",
                          textColor: ColorsHelper.redishTextColor(),
                          fontSize: 25,
                          isBold: true),
                    ),
                    Image.network(truckLoadImage),
                    // assetImageHelper(
                    //   image: ImageAssets.img_truck,
                    // )
                  ],
                ),
              SizedBox(
                height: 40,
              ),
              //Buttons
              rowButtonHelper(
                onBack: () => AppNavigator.popBackStack(context),
                onNext: () => submit(),
              ),
            ],
          ),
        ),
      );

  Widget itemListBody() => ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: itemList.length,
      itemBuilder: (context, indexitemList) {
        return ExpansionTile(
          initiallyExpanded: itemListBool[indexitemList],
          title: textHelper(
            fontSize: 13,
            title: itemList[indexitemList].estimatorTitle,
            textColor: ColorsHelper.darkBlueColor(),
          ),
          trailing: textHelper(
            fontSize: 13,
            title: itemListBool[indexitemList]
                ? StringHelper.collapse
                : StringHelper.expand,
            textColor: ColorsHelper.darkBlueColor(),
          ),
          children: <Widget>[
            GridView.builder(
                padding: EdgeInsets.all(0),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: itemList[indexitemList].images.length,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 3, crossAxisCount: 2),
                itemBuilder: (BuildContext context, int index) {
                  int boolIndex = index;
                  if (indexitemList == 0) {
                    boolIndex = index;
                  } else {
                    for (var i = 0; i < indexitemList; i++) {
                      boolIndex = boolIndex + itemList[i].images.length;
                    }
                  }
                  print(boolIndex);
                  return Center(
                    child: expandedElementsHelpers(
                      bol: step5ListBool[boolIndex],
                      text: itemList[indexitemList].images[index].title,
                      boolindex: boolIndex,
                      itemPrice: double.tryParse(
                          itemList[indexitemList].images[index].price),
                    ),
                  );
                }),
          ],
          onExpansionChanged: (bool result) {
            itemListBool[indexitemList] = result;
            setState(() {});
          },
        );
      });

  Widget truckLoadElements() => Container(
        height: 60,
        decoration: BoxDecoration(
          color: ColorsHelper.whiteColor(),
          borderRadius: BorderRadius.circular(7),
        ),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: ListView.builder(
            padding: EdgeInsets.all(0),
            shrinkWrap: true,
            itemCount: truckList.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (contet, index) {
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 5),
                child: expandedElementsHelpers(
                    bol: truckListBool[index],
                    text: truckList[index].estimatorTitle,
                    horizontal: 20,
                    boolindex: index,
                    isListItem: false,
                    itemPrice:
                        double.tryParse(truckList[index].images[0].price),
                        truckImage: truckList[index].images[0].image),
              );
            }),
      );

  Widget optionContainer(
          {bool bol,
          String text,
          double horizontal = 20,
          double vertical = 15,
          int index}) =>
      Material(
        color: bol ? ColorsHelper.redishTextColor() : ColorsHelper.whiteColor(),
        borderRadius: BorderRadius.circular(7),
        child: InkWell(
          onTap: () {
            for (var i = 0; i < 2; i++) {
              if (i == index) {
                isOpton[index] = !isOpton[index];
              } else {
                isOpton[i] = false;
              }
            }
            // for (var i = 0; i < itemListBool.length; i++) {
            //   itemListBool[i] = false;
            // }
            // for (var i = 0; i < truckListBool.length; i++) {
            //   truckListBool[i] = false;
            // }
            // for (var i = 0; i < step5ListBool.length; i++) {
            //   step5ListBool[i] = false;
            // }
            // totalPrice = 0;
            if (mounted) {
              setState(() {});
            }
          },
          borderRadius: BorderRadius.circular(7),
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: horizontal, vertical: vertical),
            child: textHelper(
              fontSize: 13,
              title: text,
              textColor: bol
                  ? ColorsHelper.whiteColor()
                  : ColorsHelper.darkBlueColor(),
            ),
          ),
        ),
      );

  Widget expandedElementsHelpers(
          {bool bol,
          String text,
          double horizontal = 10,
          double vertical = 10,
          double margin = 0,
          int boolindex,
          double itemPrice,
          String truckImage,
          bool isListItem = true}) =>
      Material(
        color: bol ? ColorsHelper.redishTextColor() : ColorsHelper.whiteColor(),
        borderRadius: BorderRadius.circular(7),
        child: InkWell(
          onTap: () {
            if (isListItem) {
              totalPriceItem = 0;
              step5ListBool[boolindex] = !step5ListBool[boolindex];
              for (var i = 0; i < step5ListBool.length; i++) {
                if (step5ListBool[i]) {
                  totalPriceItem = totalPriceItem + step5ListPrice[i];
                }
              }
            } else {
              truckLoad = text;
              truckLoadImage = truckImage;
              for (var i = 0; i < truckListBool.length; i++) {
                if (i == boolindex) {
                  truckListBool[boolindex] = !truckListBool[boolindex];
                } else {
                  truckListBool[i] = false;
                }
              }
              totalPriceTruck = itemPrice;
            }

            if (mounted) {
              setState(() {});
            }
          },
          borderRadius: BorderRadius.circular(7),
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: horizontal, vertical: vertical),
            child: textHelper(
              fontSize: 13,
              title: text,
              textColor: bol
                  ? ColorsHelper.whiteColor()
                  : ColorsHelper.darkBlueColor(),
            ),
          ),
        ),
      );
}
