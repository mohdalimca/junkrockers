import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junkrocker/src/bloc/bloc_main/main_bloc.dart';
import 'package:junkrocker/src/model/BookingQueModel.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';

class Step2Booking extends StatefulWidget {
  static const String routeName = "/Step2Booking";
  Step2Booking({Key key}) : super(key: key);

  @override
  _Step2State createState() => _Step2State();
}

class _Step2State extends State<Step2Booking> {
  List<bool> isOpton = [];
  List<BookingQueData> bookingQueList = [];
  BookingQueData bookingQueData;
  MainBloc mainBloc;

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    bookingQueList = mainBloc.bookingQueList;
    callBolfunction();
    super.initState();
  }

  callBolfunction() {
    bookingQueData = bookingQueList.firstWhere((element) {
      return element.step == 2;
    });
    for (var i = 0; i < bookingQueData.answer.length; i++) {
      isOpton.add(false);
    }
    if (mounted) {
      setState(() {});
    }
  }

  submit() {
    int index;
    for (var i = 0; i < isOpton.length; i++) {
      if (isOpton[i]) {
        index = i;
      }
    }
    if (index != null) {
      AnsModel ansModel = AnsModel(
          step: bookingQueData.step,
          questionId: int.tryParse(bookingQueData.answer[index].quesId),
          answerId: int.tryParse(bookingQueData.answer[index].id));
      mainBloc.quesAns = [];
      mainBloc.quesAns.add(jsonEncode(ansModel.toJson()));
      AppNavigator.launchStep3BookingScreen(context);
    } else {
      PopupDialogs.displayMessageOnly(context, StringHelper.select_any_one);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
            title: StringHelper.step2,
            textColor: ColorsHelper.blueButtonColor(),
            leadingIcon: null,
            onTapLeading: () => AppNavigator.popBackStack(context),
            action1Icon: ImageAssets.ic_info,
            onTapaction1: () {}),
        body: Container(
          padding: EdgeInsets.all(20),
          child: stepBody(),
        ),
      ),
    );
  }

  Widget stepBody() => ScrollConfiguration(
      behavior: ListViewScrollBehavior(),
      child: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // //are_There_Any_Large_Items
          // Text(
          //   StringHelper.are_There_Any_Large_Items,
          //   style:
          //       TextStyle(fontSize: 30, color: ColorsHelper.blueButtonColor()),
          // ),
          // SizedBox(
          //   height: 10,
          // ),
          //any_Items_of_5_feet_is_considered_large
          // Text(
          //   bookingQueData.question,
          //   // StringHelper.any_Items_of_5_feet_is_considered_large,
          //   textAlign: TextAlign.start,
          //   style:
          //       TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 18),
          // ),
          // optionContainer(bol: isOpton[0], index: 0, text: StringHelper.yes),
          // SizedBox(
          //   height: 10,
          // ),
          // optionContainer(bol: isOpton[1], index: 1, text: StringHelper.no),
          //question
          Text(
            bookingQueData.question,
            textAlign: TextAlign.start,
            style:
                TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 20),
          ),
          SizedBox(
            height: 20,
          ),
          //select_any_one
          Text(
            StringHelper.select_any_one,
            textAlign: TextAlign.start,
            style:
                TextStyle(color: ColorsHelper.blueButtonColor(), fontSize: 15),
          ),
          SizedBox(
            height: 20,
          ),
          ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: bookingQueData.answer.length,
              itemBuilder: (context, index) {
                return Column(
                  children: <Widget>[
                    optionContainer(
                        bol: isOpton[index],
                        index: index,
                        text: bookingQueData.answer[index].optionName),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                );
              }),
          SizedBox(
            height: 40,
          ),
          //Buttons
          rowButtonHelper(
              onBack: () => AppNavigator.popBackStack(context),
              onNext: () => submit()),
        ],
      )));

  Widget optionContainer({bool bol, String text, int index}) => Material(
        color: bol ? ColorsHelper.redishTextColor() : ColorsHelper.whiteColor(),
        borderRadius: BorderRadius.circular(7),
        child: InkWell(
          onTap: () {
            for (var i = 0; i < bookingQueData.answer.length; i++) {
              if (i == index) {
                isOpton[index] = !isOpton[index];
              } else {
                isOpton[i] = false;
              }
            }
            if (mounted) {
              setState(() {});
            }
          },
          borderRadius: BorderRadius.circular(7),
          child: Container(
            padding: EdgeInsets.all(12.5),
            child: Row(
              children: <Widget>[
                //CheckBox
                assetImageHelper(
                    height: 25,
                    width: 25,
                    image: bol
                        ? ImageAssets.ic_selected
                        : ImageAssets.ic_unselected02),
                SizedBox(
                  width: 20,
                ),
                textHelper(
                  title: text,
                  textColor: bol
                      ? ColorsHelper.whiteColor()
                      : ColorsHelper.darkBlueColor(),
                )
              ],
            ),
          ),
        ),
      );
}

class AnsModel {
  int step;
  int questionId;
  int answerId;

  AnsModel({this.step, this.questionId, this.answerId});

  AnsModel.fromJson(Map<String, dynamic> json) {
    step = json['step'];
    questionId = json['question_id'];
    answerId = json['answer_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['step'] = this.step;
    data['question_id'] = this.questionId;
    data['answer_id'] = this.answerId;
    return data;
  }
}