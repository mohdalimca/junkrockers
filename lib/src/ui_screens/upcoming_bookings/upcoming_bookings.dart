import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:junkrocker/src/model/BookingDetailModel.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/image_assets.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:intl/intl.dart';
import 'package:junkrocker/src/utils/app_navigator/app_navigator.dart';
import 'package:junkrocker/src/utils/common_widgets/common_widget.dart';
import 'package:junkrocker/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';

class UpcomingBookings extends StatefulWidget {
  static const String routeName = "/UpcomingBookings";
  final BookingDetailData bookingDetailData;
  UpcomingBookings({Key key, @required this.bookingDetailData})
      : super(key: key);

  @override
  _UpcomingBookingsState createState() => _UpcomingBookingsState();
}

class _UpcomingBookingsState extends State<UpcomingBookings> {
  // Completer<GoogleMapController> _controller = Completer();

  // static final CameraPosition _kGooglePlex = CameraPosition(
  //   target: LatLng(37.42796133580664, -122.085749655962),
  //   zoom: 14.4746,
  // );

  // static final CameraPosition _kLake = CameraPosition(
  //     bearing: 192.8334901395799,
  //     target: LatLng(37.43296265331129, -122.08832357078792),
  //     tilt: 59.440717697143555,
  //     zoom: 19.151926040649414);
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.backgroundColor(),
        appBar: appBarBody(
          title: "",
          leadingIcon: null,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: upcomingBookingsBody(),
        ),
      ),
    );
  }

  Widget upcomingBookingsBody() => ScrollConfiguration(
        behavior: ListViewScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //upcoming_Booking
              Text(
                StringHelper.upcoming_Booking,
                style: TextStyle(
                    fontSize: 30, color: ColorsHelper.blueButtonColor()),
              ),
              SizedBox(
                height: 10,
              ),
              //Booking data
              Column(
                children: <Widget>[
                  //itemTypeProduct & bookingDate
                  rowHelper(
                    child1: Text(
                      widget.bookingDetailData.itemTypeProduct,
                      textAlign: TextAlign.start,
                      style: TextStyle(color: ColorsHelper.blueButtonColor()),
                    ),
                    child2: Text(
                      widget.bookingDetailData.bookingDate.split(" ")[0],
                      textAlign: TextAlign.start,
                      style: TextStyle(color: ColorsHelper.greySubHeadColor()),
                    ),
                  ),
                  //bookingType
                  rowHelper(
                      child1: Text(
                        widget.bookingDetailData.bookingType,
                        textAlign: TextAlign.start,
                        style:
                            TextStyle(color: ColorsHelper.greySubHeadColor()),
                      ),
                      child2: Container(
                        height: 0,
                        width: 0,
                      )),
                  //
                  rowHelper(
                    child1: Text(
                      " ",
                      textAlign: TextAlign.start,
                      style: TextStyle(color: ColorsHelper.blueButtonColor()),
                    ),
                    child2: Text(
                      "\$ ${widget.bookingDetailData.price}",
                      textAlign: TextAlign.start,
                      style: TextStyle(color: ColorsHelper.greenDollerColor()),
                    ),
                  )
                ],
              ),
              //points
              SizedBox(
                height: 20,
              ),
              //Pick up date
              radioOption(
                  text: StringHelper.your_pickup_has_been_accepted_for +
                      DateFormat('d MMMM y').format(
                          DateTime.parse(widget.bookingDetailData.bookingDate)),
                  bol: true),
              SizedBox(
                height: 10,
              ),
              //arrival datetime
              radioOption(
                  text: StringHelper.your_juncker_will_arrive_at +
                      (widget.bookingDetailData.arrivalTime == ""
                          ? "--"
                          : (widget.bookingDetailData.arrivalTime +
                              "," +
                              DateFormat('EEEE, d MMMM y').format(
                                  DateTime.parse(
                                      widget.bookingDetailData.bookingDate)))),
                  bol: widget.bookingDetailData.arrivalTime == ""
                      ? false
                      : true),
              SizedBox(
                height: 10,
              ),
              //juncker_in_on_the_way
              radioOption(
                  text: StringHelper.juncker_in_on_the_way,
                  bol: int.tryParse(widget.bookingDetailData.trackStatus) >= 1
                      ? true
                      : false),
              SizedBox(
                height: 10,
              ),
              //your_junk_is_gone
              radioOption(
                  text: StringHelper.your_junk_is_gone,
                  bol: int.tryParse(widget.bookingDetailData.trackStatus) >= 1
                      ? true
                      : false),
              SizedBox(
                height: 20,
              ),
              //your_junk_is_gone
              radioOption(
                  text: StringHelper.completed,
                  bol: int.tryParse(widget.bookingDetailData.trackStatus) >= 1
                      ? true
                      : false),
              SizedBox(
                height: 20,
              ),
              // Container(
              //   height: 300,
              //   margin: EdgeInsets.symmetric(horizontal: 20),
              //   decoration: BoxDecoration(
              //      // color: ColorsHelper.redishTextColor(),
              //       borderRadius: BorderRadius.circular(7)),
              //   child: GoogleMap(
              //     mapType: MapType.hybrid,
              //     initialCameraPosition: _kGooglePlex,
              //     onMapCreated: (GoogleMapController controller) {
              //       _controller.complete(controller);
              //     },
              //   ),
              // ),
              //Juncker detail
              junckerDetails(),
              SizedBox(
                height: 20,
              ),
              assetImageHelper(image: ImageAssets.img_junktruck)
            ],
          ),
        ),
      );

  Widget junckerDetails() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
            color: ColorsHelper.redishTextColor(),
            borderRadius: BorderRadius.circular(7)),
        child: Row(
          children: <Widget>[
            assetImageHelper(image: ImageAssets.ic_user_, height: 30),
            SizedBox(
              width: 10,
            ),
            //name & email
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //name
                  Text(
                    widget.bookingDetailData.junkerFullname ?? "Rob Lawson",
                    style: TextStyle(color: ColorsHelper.whiteColor()),
                  ),
                  //junkerprofile
                  Text(
                    widget.bookingDetailData.junkerprofile ??
                        "Certified Junk Rocker",
                    style: TextStyle(color: ColorsHelper.greySubHeadColor()),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10,
            ),
            assetImageHelper(image: ImageAssets.ic_call, height: 30)
          ],
        ),
      );

  Widget rowHelper({Widget child1, Widget child2}) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[child1, child2],
      );

  Widget radioOption({String text, bool bol}) => Material(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(7),
        child: InkWell(
          onTap: () {
            // isHome = !isHome;
            if (mounted) {
              setState(() {});
            }
          },
          borderRadius: BorderRadius.circular(7),
          child: Container(
            padding: EdgeInsets.all(12.5),
            child: Row(
              children: <Widget>[
                //CheckBox
                assetImageHelper(
                    height: 25,
                    width: 25,
                    image: bol
                        ? ImageAssets.ic_selected
                        : ImageAssets.ic_unselected02),
                SizedBox(
                  width: 20,
                ),
                Flexible(
                  child: textHelper(
                    title: text,
                    textColor: ColorsHelper.darkBlueColor(),
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
