import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/resources/colors.dart';
import 'package:junkrocker/src/resources/fonts.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/ui_screens/on_borading_module/welcome.dart';
import 'package:junkrocker/src/ui_screens/profile_module/profile.dart';
import 'bloc/bloc_main/main_bloc.dart';
import 'ui_screens/booking_steps_module/step_2.dart';
import 'ui_screens/booking_steps_module/step_3.dart';
import 'ui_screens/booking_steps_module/step_4.dart';
import 'ui_screens/booking_steps_module/step_5.dart';
import 'ui_screens/booking_steps_module/step_6.dart';
import 'ui_screens/home_module/notifications.dart';
import 'ui_screens/on_borading_module/ForgotPassword.dart';
import 'ui_screens/on_borading_module/Login.dart';
import 'ui_screens/on_borading_module/register.dart';
import 'ui_screens/home_module/Home.dart';
import 'ui_screens/profile_module/booking_history.dart';
import 'ui_screens/profile_module/change_password.dart';
import 'ui_screens/profile_module/edit_profile.dart';
import 'ui_screens/profile_module/slug_pages.dart';
import 'ui_screens/splash/Splash.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'ui_screens/upcoming_bookings/upcoming_bookings.dart';
import 'utils/firebase_ops/fcm.dart';
import 'utils/pagerouting/slideleftroute.dart';

class App extends StatelessWidget {
  final MainBloc mainBloc = MainBloc();

  void dispose() {
    mainBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    Fcm.getInstance().chatNotification();
    print(mainBloc.name);
    mainBloc.name = "app";

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: StringHelper.app_name,
      home: BlocProvider(create: (context) => mainBloc, child: Splash()),
      theme: ThemeData(
          accentColor: ColorsHelper.whiteColor(),
          primaryColor: ColorsHelper.whiteColor(),
          scaffoldBackgroundColor: ColorsHelper.whiteColor(),
          canvasColor: ColorsHelper.whiteColor(),
          backgroundColor: ColorsHelper.whiteColor(),
          fontFamily: FontsHelper.fonts_Nunito),
      onGenerateRoute: routes,
    );
  }

  Route routes(RouteSettings settings) {
    var page;
    String routeName = settings.name;
    switch (routeName) {
      //Splash
      case Splash.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Splash());
        break;
      //Welcome
      case Welcome.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Welcome());
        break;
      //Login
      case Login.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Login());
        break;
      //ForgotPassword
      case ForgotPassword.routeName:
        page = BlocProvider(
            create: (context) => mainBloc, child: ForgotPassword());
        break;
      //Register
      case Register.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Register());
        break;
      //Home
      case Home.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Home());
        break;
      //Profile
      case Profile.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Profile());
        break;
      //EditProfile
      case EditProfile.routeName:
        page =
            BlocProvider(create: (context) => mainBloc, child: EditProfile());
        break;
      //BookingHistory
      case BookingHistory.routeName:
        page = BlocProvider(
            create: (context) => mainBloc,
            child: BookingHistory(
              bookingHistoryList: settings.arguments,
            ));
        break;
      //Step2Booking
      case Step2Booking.routeName:
        page =
            BlocProvider(create: (context) => mainBloc, child: Step2Booking());
        break;
      //Step3Booking
      case Step3Booking.routeName:
        page =
            BlocProvider(create: (context) => mainBloc, child: Step3Booking());
        break;
      //Step4Booking
      case Step4Booking.routeName:
        page =
            BlocProvider(create: (context) => mainBloc, child: Step4Booking());
        break;
      //Step5Booking
      case Step5Booking.routeName:
        page =
            BlocProvider(create: (context) => mainBloc, child: Step5Booking());
        break;
      //Step6Booking
      case Step6Booking.routeName:
        page =
            BlocProvider(create: (context) => mainBloc, child: Step6Booking());
        break;
      //UpcomingBookings
      case UpcomingBookings.routeName:
        page = BlocProvider(
            create: (context) => mainBloc,
            child: UpcomingBookings(
              bookingDetailData: settings.arguments,
            ));
        break;
      //AboutUs
      case SlugPage.routeName:
        page = BlocProvider(
            create: (context) => mainBloc,
            child: SlugPage(
              pageData: settings.arguments,
            ));
        break;
      //Notifications
      case Notifications.routeName:
        page =
            BlocProvider(create: (context) => mainBloc, child: Notifications());
        break;
      //ChangePassword
      case ChangePassword.routeName:
        page = BlocProvider(
            create: (context) => mainBloc, child: ChangePassword());
        break;
    }

    return SlideLeftRoute(
        widget:
            // StreamBuilder(
            //     stream: ConnectionService.getInstance().connectionStatus,
            //     builder:
            //         (BuildContext context, AsyncSnapshot<dynamic> netConnection) {
            //       if (netConnection.data != null) {
            //         return netConnection.data ? page : NoNetConnection();
            //       } else {
            //         return NoNetConnection();
            //       }
            //     })
            page);
  }
}
