import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/repository/booking_step_repository.dart';
import 'package:junkrocker/src/data/repository/common_repository.dart';
import 'package:junkrocker/src/model/BookingQueModel.dart';
import 'package:junkrocker/src/model/Step5Model.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';
part 'main_event.dart';
part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  String name = "Ram";
  List<BookingQueData> bookingQueList = [];
  List<Step5Data> step5List = [];
  List<Step5Data> itemList = [];
  List<Step5Data> truckList = [];

  Future<bool> callBookingQuesApi({BuildContext context}) async {
    bookingQueList =
        await CommonRepository.bookingQuesApiCall(context: context);
    return bookingQueList != null;
  }

  Future<bool> callstep5DataApi({BuildContext context}) async {
    step5List = await CommonRepository.step5DataApiCall(context: context);
    itemList = [];
    truckList = [];
    for (var i = 0; i < step5List.length; i++) {
      if (step5List[i].itemType == "1") {
        itemList.add(step5List[i]);
      } else {
        truckList.add(step5List[i]);
      }
    }
    return step5List != null;
  }

  @override
  MainState get initialState => MainInitial();

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {}
  //Do booking
  int userId;
  int zipcode;
  List quesAns;
  String bookingType;
  String address;
  String states;
  int phoneNumber;
  String slotDate;
  String slotTime;
  String extraInformation;
  int itemType;
  double price;
  String itemTypeProduct;
  int isPickupAddress;
  int isNotify;
  String addressType;
  String cardToken;

  Future<dynamic> callDoBookingApi(BuildContext context) async {
    userId = await SharePreferencesHelper.getInstant()
        .getInt(SharePreferencesHelper.Id);
    var result = await BookingStepRepository.doBookingApiCall(
        context: context,
        userId: userId,
        zipcode: zipcode,
        quesAns: quesAns,
        bookingType: bookingType,
        address: address,
        // states: states,
        phoneNumber: phoneNumber,
        slotDate: slotDate,
        slotTime: slotTime,
        extraInformation: extraInformation,
        itemType: itemType,
        price: price,
        itemTypeProduct: itemTypeProduct,
        isPickupAddress: isPickupAddress,
        isNotify: isNotify,
        addressType: addressType,
        cardToken: cardToken);
    return result;
  }
}
