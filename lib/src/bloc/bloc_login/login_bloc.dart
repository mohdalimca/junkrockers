import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/network/api.dart';
import 'package:junkrocker/src/data/repository/login_repository.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:junkrocker/src/model/LoginModel.dart';
part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  bool autoValidation = false;

  //onLoginClick
  Future<bool> onLoginClick(final formKey) async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      return true;
    } else {
      autoValidation = true;
      return false;
    }
  }

  //SignUp
  Future<dynamic> callSignUpApi(
      {String fullName,
      String email,
      String password,
      BuildContext context}) async {
    var result = await LoginRepository.signUpApiCall(
        fullName: fullName, email: email, password: password, context: context);
    return result;
  }

  //Login
  Future<bool> callLoginApi(
      {String email, String password, BuildContext context}) async {
    LoginData result = await LoginRepository.loginApiCall(
        email: email, password: password, context: context);
    if (result != null) {
      SharePreferencesHelper.getInstant()
          .setInt(SharePreferencesHelper.Id, int.tryParse(result.id));
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Full_Name, result.fullName);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Email, result.email);
      // SharePreferencesHelper.getInstant()
      //     .setString(SharePreferencesHelper.Password, result.password);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Phone, result.phone);
      if (result.profileImage != "") {
        SharePreferencesHelper.getInstant().setString(
            SharePreferencesHelper.Profile_Pic,
            Api.Image_Url + result.profileImage);
      }
      return true;
    } else {
      return false;
    }
  }

  //ForgotPassword
  Future<dynamic> callForgotPasswordApi(
      {String email, BuildContext context}) async {
    var result = await LoginRepository.forgotPasswordApiCall(
        email: email, context: context);
    return result;
  }

  @override
  LoginState get initialState => LoginInitState();

  final _streamController = StreamController<LoginState>();
  Stream<LoginState> get stream => _streamController.stream;

  dispose() {
    _streamController.close();
  }

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {}
}
