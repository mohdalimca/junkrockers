part of 'login_bloc.dart';

class LoginState {
  LoginState();
  factory LoginState.initState() = LoginInitState;
  factory LoginState.loadingState() = LoginLoadingState;
  factory LoginState.failuerState(String message) = LoginFailureState;
  factory LoginState.successState(String message) = LoginSuccesState;
}

class LoginInitState extends LoginState {}

class LoginLoadingState extends LoginState {}

class LoginFailureState extends LoginState {
  final String message;
  LoginFailureState(this.message);
}

class LoginSuccesState extends LoginState {
  final String message;
  LoginSuccesState(this.message);
}
