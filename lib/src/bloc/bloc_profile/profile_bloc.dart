import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:junkrocker/src/data/network/api.dart';
import 'package:junkrocker/src/data/repository/profile_repository.dart';
import 'package:junkrocker/src/model/BookingHistoryModel.dart';
import 'package:junkrocker/src/model/UserDetailModel.dart';
import 'package:junkrocker/src/model/PagesModel.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:rxdart/rxdart.dart';
part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  bool autoValidation = false;

  //onLoginClick
  Future<bool> onLoginClick(final formKey) async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      return true;
    } else {
      autoValidation = true;
      return false;
    }
  }

  //UpdateProfile
  Future<dynamic> callUpdateProfileApi(
      {int id,
      File profileImage,
      String fullName,
      BuildContext context}) async {
    var result = await ProfileRepository.updateProfileApiCall(
        id: id,
        profileImage: profileImage,
        fullName: fullName,
        context: context);
    if (result != null) {
      bool update = await callUserDetailApi(userId: id, context: context);
      if (update) {
        return result;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  //User Detail
  Future<bool> callUserDetailApi({int userId, BuildContext context}) async {
    List<UserDetailData> userDetailList =
        await ProfileRepository.userDetailApiCall(
            userId: userId, context: context);
    if (userDetailList != null) {
      SharePreferencesHelper.getInstant().setInt(
          SharePreferencesHelper.Id, int.tryParse(userDetailList[0].id));
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Full_Name, userDetailList[0].fullName);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Email, userDetailList[0].email);
      // SharePreferencesHelper.getInstant().setString(
      //     SharePreferencesHelper.Password, userDetailList[0].password);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Phone, userDetailList[0].phone);
      if (userDetailList[0].profileImage != "") {
        SharePreferencesHelper.getInstant().setString(
            SharePreferencesHelper.Profile_Pic,
            Api.Image_Url + userDetailList[0].profileImage);
      }

      return true;
    } else {
      return false;
    }
  }

  //Pages Data
  Future<List<PagesData>> callPagesDataApi(
      {String pageSlug, BuildContext context}) async {
    var result = await ProfileRepository.pagesDataApiCall(
        pageSlug: pageSlug, context: context);
    return result;
  }

  //Change Passowrd
  Future<dynamic> callChangePasswordApi(
      {int id,
      String oldPwd,
      String newPwd,
      String cNewPwd,
      BuildContext context}) async {
    var result = await ProfileRepository.changePasswordApiCall(
        id: id,
        oldPwd: oldPwd,
        newPwd: newPwd,
        cNewPwd: cNewPwd,
        context: context);
    return result;
  }

  // Booking History
  Future<List<BookingHistoryData>> callBookingHistoryApi(
      {int userId, BuildContext context}) async {
    var result = await ProfileRepository.bookingHistoryApiCall(
        userId: userId, context: context);
    return result;
  }

  //Logout
  Future<dynamic> callLogoutApi({BuildContext context}) async {
    var result = await ProfileRepository.pagesDataApiCall(context: context);
    return result;
  }

  File imageFile;
  BehaviorSubject<File> image = BehaviorSubject<File>();
  Stream<File> get imageHere {
    return image.stream;
  }

  void dispose() {
    image.close();
  }

  //experiment
  void imagePicker(BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        context: context,
        builder: (builder) {
          return Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Wrap(
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(12),
                    child: Text(
                      StringHelper.str_choose_from,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          // fontFamily: FontsHelper.fonts_Nanum_Barun_Gothic,
                          fontWeight: FontWeight.w700,
                          fontSize: 20),
                    ),
                  ),
                ),
                ListTile(
                    leading: Icon(
                      Icons.camera,
                      color: Colors.black,
                    ),
                    title: Text(
                      StringHelper.str_camera,
                      style: TextStyle(color: Colors.black),
                    ),
                    onTap: () {
                      _onCameraClick(context);
                    }),
                ListTile(
                    leading: Icon(
                      Icons.image,
                      color: Colors.black,
                    ),
                    title: Text(
                      StringHelper.str_gallery,
                      style: TextStyle(color: Colors.black),
                    ),
                    onTap: () {
                      _onGalleryClick(context);
                    }),
              ],
            ),
          );
        });
  }

  Future _onCameraClick(BuildContext context) async {
    Navigator.pop(context);
    imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    if (imageFile != null) {
      image.sink.add(imageFile);
    }
  }

  Future _onGalleryClick(BuildContext context) async {
    Navigator.pop(context);
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile != null) {
      image.sink.add(imageFile);
      // File croppedFile = await ImageCropper.cropImage(
      //     sourcePath: imageFile.path,
      //     aspectRatioPresets: Platform.isAndroid
      //         ? [
      //             CropAspectRatioPreset.square,
      //             CropAspectRatioPreset.ratio3x2,
      //             CropAspectRatioPreset.original,
      //             CropAspectRatioPreset.ratio4x3,
      //             CropAspectRatioPreset.ratio16x9
      //           ]
      //         : [
      //             CropAspectRatioPreset.original,
      //             CropAspectRatioPreset.square,
      //             CropAspectRatioPreset.ratio3x2,
      //             CropAspectRatioPreset.ratio4x3,
      //             CropAspectRatioPreset.ratio5x3,
      //             CropAspectRatioPreset.ratio5x4,
      //             CropAspectRatioPreset.ratio7x5,
      //             CropAspectRatioPreset.ratio16x9
      //           ],
      //     androidUiSettings: AndroidUiSettings(
      //         toolbarTitle: 'Crop Image',
      //         toolbarColor: Colors.deepOrange,
      //         toolbarWidgetColor: Colors.white,
      //         initAspectRatio: CropAspectRatioPreset.original,
      //         lockAspectRatio: false),
      //     iosUiSettings: IOSUiSettings(
      //       title: 'Crop Image',
      //     ));
      // if (croppedFile != null) {
      //   image.sink.add(croppedFile);
      // }
    }
  }

  void deleteImage() {
    imageFile = null;
    image.sink.add(imageFile);
  }

  @override
  ProfileState get initialState => ProfileInitial();

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {}
}
