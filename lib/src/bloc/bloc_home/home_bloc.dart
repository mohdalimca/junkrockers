import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/repository/home_repository.dart';
import 'package:junkrocker/src/model/BookingDetailModel.dart';
import 'package:junkrocker/src/model/UpcomingBookingModel.dart';
import 'package:junkrocker/src/model/ZipCodeModel.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';
part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  //ZipCode Data
  Future<List<ZipCodeData>> callZipCodeApi(
      {String zipcode, BuildContext context}) async {
    var result = await HomeeRepository.zipCodeApiCall(
        zipcode: zipcode, context: context);
    return result;
  }

  //upcomingBookings
  Future<List<UpcomingBookingData>> callUpcomingBookingsApi(
      {BuildContext context}) async {
    int userId = await SharePreferencesHelper.getInstant()
        .getInt(SharePreferencesHelper.Id);
    var result = await HomeeRepository.upcomingBookingsApiCall(
        userId: userId, context: context);
    return result;
  }

  // Booking Detail
  Future<List<BookingDetailData>> callBookingDetailApi(
      {int bookingId, BuildContext context}) async {
    var result = await HomeeRepository.bookingDetailApiCall(
        bookingId: bookingId, context: context);
    return result;
  }

  @override
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {}
}
