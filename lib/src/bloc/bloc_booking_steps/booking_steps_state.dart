part of 'booking_steps_bloc.dart';

abstract class BookingStepsState extends Equatable {
  const BookingStepsState();
}

class BookingStepsInitial extends BookingStepsState {
  @override
  List<Object> get props => [];
}
