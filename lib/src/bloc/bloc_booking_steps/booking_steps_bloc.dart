import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/repository/booking_step_repository.dart';
import 'package:junkrocker/src/model/SlotBookingsModel.dart';
// import 'package:stripe_payment/stripe_payment.dart';
part 'booking_steps_event.dart';
part 'booking_steps_state.dart';

class BookingStepsBloc extends Bloc<BookingStepsEvent, BookingStepsState> {
  bool autoValidation = false;

  //onLoginClick
  Future<bool> onLoginClick(final formKey) async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      return true;
    } else {
      autoValidation = true;
      return false;
    }
  }

  //Booking Slot List
  Future<List<SlotBookingsData>> callBookingSlotsApi(
      {BuildContext context, String slotDate}) {
    var result = BookingStepRepository.bookingSlotsApiCall(
        context: context, slotDate: slotDate);
    return result;
  }

  @override
  BookingStepsState get initialState => BookingStepsInitial();

  @override
  Stream<BookingStepsState> mapEventToState(
    BookingStepsEvent event,
  ) async* {}

  //Payment Code

  String error = "Some error occurred";

  // Token token;
  String publishKey = "pk_test_aSaULNS8cJU6Tvo20VAXy6rp";
  String secretKey = '';
  TextEditingController cardnum = TextEditingController();

  String newText = '';

  BookingStepsBloc() {
    // StripePayment.setOptions(StripeOptions(
    //     publishableKey: publishKey,
    //     merchantId: "Test",
    //     androidPayMode: 'test'));
  }

  // Future<Token> addCard(BuildContext context) async {
  //   GlobalKey<FormState> formKey = GlobalKey();
  //   CardDetail cardDetails1 = CardDetail(number: 0, expMonth: 12, expYear: 34);
  //   CardDetail cardDetails = await showDialog<CardDetail>(
  //       context: context,
  //       builder: (context) {
  //         return AlertDialog(
  //           title: Text("Add Card Details"),
  //           content: Container(
  //             height: 250,
  //             child: Form(
  //                 key: formKey,
  //                 child: Column(children: <Widget>[
  //                   Text("Card Number"),
  //                   Container(
  //                     height: 70,
  //                     width: MediaQuery.of(context).size.width * 0.7,
  //                     child: TextFormField(
  //                       maxLength: 16,
  //                       keyboardType: TextInputType.number,
  //                       controller: cardnum,
  //                       validator: (number) =>
  //                           number.length == 16 ? null : "Invalid card number",
  //                       onChanged: (number) {
  //                         cardDetails1.number =
  //                             int.tryParse(number.replaceAll('-', '')) ?? 0;
  //                       },
  //                     ),
  //                   ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                     children: <Widget>[
  //                       Text("Expiry Month"),
  //                       Text("Expiry Year")
  //                     ],
  //                   ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                     children: <Widget>[
  //                       Container(
  //                         height: 70,
  //                         width: 67,
  //                         child: TextFormField(
  //                           maxLength: 2,
  //                           keyboardType: TextInputType.number,
  //                           validator: (number) =>
  //                               number.length == 2 ? null : "Invalid value",
  //                           onChanged: (number) => cardDetails1.expMonth =
  //                               int.tryParse(number) ?? 0,
  //                         ),
  //                       ),
  //                       Container(
  //                         height: 70,
  //                         width: 67,
  //                         child: TextFormField(
  //                           //maxLength: 2,
  //                           keyboardType: TextInputType.number,
  //                           maxLength: 2,
  //                           validator: (number) =>
  //                               number.length == 2 ? null : "Invalid value",
  //                           onChanged: (number) => cardDetails1.expYear =
  //                               int.tryParse(number) ?? 0,
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ])),
  //           ),
  //           actions: [
  //             FlatButton(
  //               child: Text("Done"),
  //               onPressed: () {
  //                 if (!formKey.currentState.validate())
  //                   return 8;
  //                 else
  //                   Navigator.of(context).pop(cardDetails1);
  //                 return 4;
  //               },
  //             ),
  //             FlatButton(
  //               child: Text("Cancel"),
  //               onPressed: () {
  //                 Navigator.of(context).pop();
  //                 cardnum.clear();
  //               },
  //             ),
  //           ],
  //         );
  //       });
  //   if (cardDetails == null) return null;
  //   try {
  //     LoadingWidget.startLoadingWidget(context);
  //     print(
  //         "getting token ${cardDetails.number} ${cardDetails.expMonth} ${cardDetails.expYear}");
  //     Token token = await StripePayment.createTokenWithCard(CreditCard(
  //         number: cardDetails.number.toString(),
  //         expMonth: cardDetails.expMonth,
  //         expYear: cardDetails.expYear));
  //     LoadingWidget.endLoadingWidget(context);
  //     this.token = token;
  //     print(token);
  //   } on Exception catch (e) {
  //     LoadingWidget.endLoadingWidget(context);
  //     setError(e);
  //   }
  //   if (this.token == null || this.token.tokenId.isEmpty)
  //     return null;
  //   else
  //     return this.token;
  // }

  void setError(dynamic error) {
    error = error.toString();
  }
}

//CardDetails
class CardDetail {
  int number;
  int expMonth;
  int expYear;
  CardDetail({this.number, this.expMonth, this.expYear});
}
