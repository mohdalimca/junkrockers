import 'dart:async';
import 'dart:collection';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:junkrocker/src/resources/strings.dart';
import 'api.dart';
import 'api_response.dart';

class RestClientNew {
  Dio _dio;

  RestClientNew() {
    BaseOptions options = new BaseOptions(
      baseUrl: Api.BASE_URL,
    );
    _dio = Dio(options);
    _dio.interceptors
        .add(LogInterceptor(requestBody: true, responseBody: true));
  }

  Future<ApiResponse> get(BuildContext context, apiName,
      {body, Map<String, dynamic> header, Options option}) async {
    Map<String, dynamic> headers = HashMap();

    if (header != null) {
      headers.addAll(header);
    }

    if (option != null) {
      option.headers = headers;
    } else {
      option = Options(method: "get");
      option.headers = headers;
    }

    try {
      Response response =
          await _dio.request(apiName, data: body, options: option);

      if (response.statusCode < 200 || response.statusCode >= 400) {
        return ApiResponse(false, 'Server error please try again later!', null,
            null, null, null, null, false, null);
      }

      return ApiResponse.fromJson(response.data);
    } catch (error) {
      if (error.response.statusCode == 400) {
        return ApiResponse(false, error.response.data.toString(), null, null,
            null, null, null, false, null);
      }
      return ApiResponse(false, 'Server error please try again later!', null,
          null, null, null, null, false, null);
    }
  }

  Future<ApiResponse> post(
    BuildContext context,
    apiName, {
    body,
    Map<String, dynamic> header,
    Options option,
  }) async {
    Map<String, dynamic> headers = HashMap();

    if (header != null) {
      headers.addAll(header);
    }

    if (option != null) {
      option.headers = headers;
    } else {
      option = Options(method: "post");
      option.headers = headers;
    }
    Response response;
    try {
      response = await _dio.request(
        apiName,
        data: body,
        options: option,
        onSendProgress: (int sent, int total) {
          print("Sent  = " +
              sent.toString() +
              " Left = " +
              (total - sent).toString());
        },
      );
      return ApiResponse.fromJson(response.data);
    } catch (e) {
      var error = e as DioError;
      if (error.response.statusCode == 401) {
        // doLogout(context);
        return ApiResponse(false, StringHelper.session_expired_please, null,
            null, error.response.statusCode, null, null, false, null);
      } else if (error.response.statusCode == 535) {
        return ApiResponse.fromJson(error.response.data);
      } else if (error.response.statusCode == 404) {
        return ApiResponse(false, error.response.data.toString(), null, null,
            null, null, null, false, null);
      } else if (error.response.statusCode == 400) {
        return ApiResponse(false, error.response.data.toString(), null, null,
            null, null, null, false, null);
      }
      return ApiResponse(false, 'Server error please try again later!', null,
          null, null, null, null, false, null);
    }
  }

  Future<ApiResponse> put(BuildContext context, apiName,
      {body, Map<String, dynamic> header, Options option}) async {
    Map<String, dynamic> headers = HashMap();

    if (header != null) {
      headers.addAll(header);
    }

    if (option != null) {
      option.headers = headers;
    } else {
      option = Options(method: "put");
      option.headers = headers;
    }

    try {
      Response response =
          await _dio.request(apiName, data: body, options: option);

      if (response.statusCode < 200 || response.statusCode > 400) {
        return ApiResponse(false, 'Server error please try again later!', null,
            null, null, null, null, false, null);
      }

      return ApiResponse.fromJson(response.data);
    } catch (e) {
      var error = e as DioError;
      if (error.response.statusCode == 401) {
        // doLogout(context);
        //   AppNavigator.launchAuthFailedPage(context);
      }
      return ApiResponse(false, 'Server error please try again later!', null,
          null, null, null, null, false, null);
    }
  }

  // void doLogout(BuildContext context) async {
  //   SharePreferencesHelper.getInstant()
  //       .setBool(SharePreferencesHelper.Is_Login, false);
  //   SharePreferencesHelper.getInstant().clearPreference();
  //   AppNavigator.launchLoginPage(context);
  // }
}

final RestClientNew restClientNew = RestClientNew();
