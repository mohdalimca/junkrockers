class ApiResponse {
  bool status;
  String message;
  dynamic data;
  dynamic otherData;
  int statusCode;
  int requestStatus;
  String token;
  bool success;
  dynamic itemsNo;

  ApiResponse(
      this.status,
      this.message,
      this.data,
      this.otherData,
      this.statusCode,
      this.requestStatus,
      this.token,
      this.success,
      this.itemsNo);

  ApiResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'].toString();
    data = json['data'];
    otherData = json['other_data'];
    statusCode = json['status_code'];
    requestStatus = json['request_status'];
    token = json['token'];
    success = json['success'];
    itemsNo = json['items_no'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = new Map<String, dynamic>();
    map['status'] = this.status;
    map['message'] = this.message;
    map['data'] = this.data;
    map['other_data'] = otherData;
    map['status_code'] = statusCode;
    map['request_status'] = requestStatus;
    map['token'] = token;
    map['success'] = success;
    map['items_no'] = itemsNo;
    return map;
  }
}
