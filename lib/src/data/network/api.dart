class Api {
  static const BASE_URL =
      "http://ec2-3-6-87-120.ap-south-1.compute.amazonaws.com/api";

  static const Image_Url =
      "http://ec2-3-6-87-120.ap-south-1.compute.amazonaws.com/upload/";    

  //Common Api's
  static const booking_info_que = "/authentication/bookinginfo";
  static const step_5_estimater = "/authentication/estimater";

  //Login_sign_Up
  static const registration = "/authentication/registration";
  static const login = "/authentication/login";
  static const logout = "/authentication/logout";
  static const change_Password = "/authentication/changePassword";
  static const forgot_Password = "/authentication/forgotPassword";

  //Profile
  static const profile_update = "/authentication/profileupdate";
  static const user_detail = "/authentication/userdetail";
  static const page = "/pages/page/";
  static const booking_history = "/authentication/bookinghistory";

  //Home
  static const postcode_search = "/postcode/search";
  static const upcomingbooking = "/authentication/upcomingbooking";

  //Booking Steps
  static const booking_Slot = "/authentication/slot";
  static const do_booking = "/authentication/bookslot";

  //Upcoming Booking
  static const booking_detail = "/authentication/bookingdetail";
}
