import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/network/api.dart';
import 'package:junkrocker/src/data/network/api_response.dart';
import 'package:junkrocker/src/data/network/rest_client_new.dart';
import 'package:junkrocker/src/model/SlotBookingsModel.dart';
import 'package:junkrocker/src/utils/loading_widget.dart/loading_widget.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';

class BookingStepRepository {
  //Booking Slot List
  static Future<List<SlotBookingsData>> bookingSlotsApiCall(
      {BuildContext context, String slotDate}) async {
    FormData formData = FormData.fromMap({'slot_date': slotDate});
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.booking_Slot, body: formData);
      if (apiResponse.status) {
        List<SlotBookingsData> slotBookingsList = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          slotBookingsList.add(SlotBookingsData.fromJson(apiResponse.data[i]));
        }
        return slotBookingsList;
      } else {
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  //do Booking
  static Future<dynamic> doBookingApiCall({
    BuildContext context,
    int userId,
    int zipcode,
    List quesAns,
    String bookingType,
    String address,
    String states,
    int phoneNumber,
    String slotDate,
    String slotTime,
    String extraInformation,
    int itemType,
    double price,
    String itemTypeProduct,
    int isPickupAddress,
    int isNotify,
    String addressType,
    String cardToken,
  }) async {
    FormData formData = FormData.fromMap({
      'user_id': userId,
      'zipcode': zipcode,
      'quesAns': quesAns.toString(),
      'booking_type': bookingType,
      'address': address,
      //'state': states,
      'phone_number': phoneNumber,
      'slotdate': slotDate,
      'slottime': slotTime,
      'extra_information': extraInformation,
      'item_type': itemType,
      'price': price,
      'item_type_product': itemTypeProduct,
      'is_pickup_address': isPickupAddress,
      'is_notify': isNotify,
      'address_type': addressType,
      'card_token':cardToken
    });
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.do_booking, body: formData);
      if (apiResponse.status) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }
}
