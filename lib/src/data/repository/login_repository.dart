import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/network/api.dart';
import 'package:junkrocker/src/model/LoginModel.dart';
import 'package:junkrocker/src/data/network/api_response.dart';
import 'package:junkrocker/src/data/network/rest_client_new.dart';
import 'package:junkrocker/src/utils/loading_widget.dart/loading_widget.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:junkrocker/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class LoginRepository {
  //sign Up
  static Future<dynamic> signUpApiCall(
      {String fullName,
      String email,
      String password,
      BuildContext context}) async {
    FormData formData = FormData.fromMap(
        {"full_name": fullName, "email": email, "password": password});
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.registration, body: formData);
      if (apiResponse.status) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //login
  static Future<LoginData> loginApiCall(
      {String email, String password, BuildContext context}) async {
    FormData formData =
        FormData.fromMap({"email": email, "password": password});
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.login, body: formData);
      if (apiResponse.status) {
        SharePreferencesHelper.getInstant().setAccessToken(
            SharePreferencesHelper.Access_Token, apiResponse.token);
        LoginData loginData;
        loginData = LoginData.fromJson(apiResponse.data);
        LoadingWidget.endLoadingWidget(context);
        return loginData;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //forgotPassword
  static Future<dynamic> forgotPasswordApiCall(
      {String email, BuildContext context}) async {
    FormData formData = FormData.fromMap({
      "email": email,
    });
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.forgot_Password, body: formData);
      if (apiResponse.status) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }
}
