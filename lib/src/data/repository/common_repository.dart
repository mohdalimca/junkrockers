import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/network/api.dart';
import 'package:junkrocker/src/data/network/api_response.dart';
import 'package:junkrocker/src/data/network/rest_client_new.dart';
import 'package:junkrocker/src/model/BookingQueModel.dart';
import 'package:junkrocker/src/model/Step5Model.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';

class CommonRepository {
  //Check App version
  static Future<dynamic> checkAppVersionApiCall({BuildContext context}) async {
    try {
      ApiResponse apiResponse = await restClientNew.get(
        context,
        Api,
      );
      if (apiResponse.status) {
        return apiResponse.data;
      } else {
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  //Booking Ques
  static Future<List<BookingQueData>> bookingQuesApiCall(
      {BuildContext context}) async {
    try {
      ApiResponse apiResponse = await restClientNew.post(
        context,
        Api.booking_info_que,
      );
      if (apiResponse.status) {
        List<BookingQueData> bookingQueList = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          bookingQueList.add(BookingQueData.fromJson(apiResponse.data[i]));
        }
        return bookingQueList;
      } else {
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  //step5Data
  static Future<List<Step5Data>> step5DataApiCall(
      {BuildContext context}) async {
    try {
      ApiResponse apiResponse = await restClientNew.post(
        context,
        Api.step_5_estimater,
      );
      if (apiResponse.status) {
        List<Step5Data> step5List = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          step5List.add(Step5Data.fromJson(apiResponse.data[i]));
        }
        return step5List;
      } else {
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
