import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/network/api.dart';
import 'package:junkrocker/src/model/BookingHistoryModel.dart';
import 'package:junkrocker/src/model/UserDetailModel.dart';
import 'package:junkrocker/src/model/PagesModel.dart';
import 'package:junkrocker/src/data/network/api_response.dart';
import 'package:junkrocker/src/data/network/rest_client_new.dart';
import 'package:junkrocker/src/utils/loading_widget.dart/loading_widget.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';

class ProfileRepository {
  //update Profile
  static Future<dynamic> updateProfileApiCall(
      {int id,
      File profileImage,
      String fullName,
      BuildContext context}) async {
    FormData formData = FormData.fromMap({
      'id': id,
      'profile_image': profileImage != null
          ? await MultipartFile.fromFile(profileImage.path,
              filename: profileImage.path)
          : null,
      'full_name': fullName,
    });
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.profile_update, body: formData);
      if (apiResponse.status) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //userDetail
  static Future<List<UserDetailData>> userDetailApiCall(
      {int userId, BuildContext context}) async {
    FormData formData = FormData.fromMap({'user_id': userId});
    // LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.user_detail, body: formData);
      if (apiResponse.status) {
        List<UserDetailData> userDetailList = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          userDetailList.add(UserDetailData.fromJson(apiResponse.data[i]));
        }
        //  LoadingWidget.endLoadingWidget(context);
        return userDetailList;
      } else {
        //  LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      // LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //pagesData
  static Future<List<PagesData>> pagesDataApiCall(
      {String pageSlug, BuildContext context}) async {
    FormData formData = FormData.fromMap({'page_slug': pageSlug});
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.page, body: formData);
      if (apiResponse.status) {
        List<PagesData> pagesDataList = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          pagesDataList.add(PagesData.fromJson(apiResponse.data[i]));
        }
        LoadingWidget.endLoadingWidget(context);
        return pagesDataList;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //Change Password
  static Future<dynamic> changePasswordApiCall(
      {int id,
      String oldPwd,
      String newPwd,
      String cNewPwd,
      BuildContext context}) async {
    FormData formData = FormData.fromMap({
      "userId": id,
      "oldPassword": oldPwd,
      "newPassword": newPwd,
      "cNewPassword": cNewPwd
    });
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew
          .post(context, Api.change_Password, body: formData);
      if (apiResponse.status) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //Booking History
  static Future<List<BookingHistoryData>> bookingHistoryApiCall(
      {int userId, BuildContext context}) async {
    FormData formData = FormData.fromMap({'user_id': userId});
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew
          .post(context, Api.booking_history, body: formData);
      if (apiResponse.status) {
        List<BookingHistoryData> bookingHistoryList = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          bookingHistoryList
              .add(BookingHistoryData.fromJson(apiResponse.data[i]));
        }
        LoadingWidget.endLoadingWidget(context);
        return bookingHistoryList;
      } else {
        LoadingWidget.endLoadingWidget(context);
       // PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //logout
  static Future<dynamic> logoutApiCall({BuildContext context}) async {
    // LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew.post(context, Api.logout);
      if (apiResponse.status) {
        //  LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        // LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      //LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }
}
