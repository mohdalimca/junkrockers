import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:junkrocker/src/data/network/api.dart';
import 'package:junkrocker/src/model/BookingDetailModel.dart';
import 'package:junkrocker/src/data/network/api_response.dart';
import 'package:junkrocker/src/data/network/rest_client_new.dart';
import 'package:junkrocker/src/model/UpcomingBookingModel.dart';
import 'package:junkrocker/src/model/ZipCodeModel.dart';
import 'package:junkrocker/src/utils/loading_widget.dart/loading_widget.dart';
import 'package:junkrocker/src/utils/popup_dialogs/popup_dialogs.dart';

class HomeeRepository {
  //ZipCode
  static Future<List<ZipCodeData>> zipCodeApiCall(
      {String zipcode, BuildContext context}) async {
    FormData formData = FormData.fromMap({'zipcode': zipcode});
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew
          .post(context, Api.postcode_search, body: formData);
      if (apiResponse.status) {
        List<ZipCodeData> zipCodeDataList = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          zipCodeDataList.add(ZipCodeData.fromJson(apiResponse.data[i]));
        }
        LoadingWidget.endLoadingWidget(context);
        return zipCodeDataList;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //UpcomingBookings
  static Future<List<UpcomingBookingData>> upcomingBookingsApiCall(
      {int userId, BuildContext context}) async {
    FormData formData = FormData.fromMap({'user_id': userId});
    // LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew
          .post(context, Api.upcomingbooking, body: formData);
      if (apiResponse.status) {
        List<UpcomingBookingData> upcomingBookingList = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          upcomingBookingList
              .add(UpcomingBookingData.fromJson(apiResponse.data[i]));
        }
        // LoadingWidget.endLoadingWidget(context);
        return upcomingBookingList;
      } else {
        // LoadingWidget.endLoadingWidget(context);
        // PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      // LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }

  //Booking Detail
  static Future<List<BookingDetailData>> bookingDetailApiCall(
      {int bookingId, BuildContext context}) async {
    FormData formData = FormData.fromMap({'booking_id': bookingId});
     LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew
          .post(context, Api.booking_detail, body: formData);
      if (apiResponse.status) {
        List<BookingDetailData> bookingDetailList = [];
        for (var i = 0; i < apiResponse.data.length; i++) {
          bookingDetailList
              .add(BookingDetailData.fromJson(apiResponse.data[i]));
        }
         LoadingWidget.endLoadingWidget(context);
        return bookingDetailList;
      } else {
         LoadingWidget.endLoadingWidget(context);
         PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
       LoadingWidget.endLoadingWidget(context);
      return null;
    }
  }
}
